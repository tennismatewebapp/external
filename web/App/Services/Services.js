//Service bootstrapper
'use strict';
/***********************************************************************************************************************************************************************/

/***********************************************************************************************************************************************************************/
tmApp.service("amsService", ['$rootScope', 'localStorageService', '$q', '$timeout', function($rootScope, localStorageService, $q, $timeout){
    //API Client
    var self = this;

    this.init = function(){
        var ams = {
            url: "https://tennismate.azure-mobile.net/",//Live: "https://tennismate.azure-mobile.net/" //Local: "http://localhost/tennismateService/"
            key: "BayVMCCnsbEDypqsbKJZKzoRSGBDKG94"
        };
        $rootScope.apiClient = new WindowsAzure.MobileServiceClient(ams.url, ams.key); 
    };

    this.Client = function(){     
        if(!$rootScope.apiClient){
            self.init();
        }
        return $rootScope.apiClient;         
    };

    this.invokeAPI = function (APIName, method, input, output){
        var deferred = $q.defer();
        $rootScope.isLoading = true;
        self.Client().invokeApi(APIName, {
            body: input,
            method: method
        }).done(function (response) {
            output.val = response.result;
            $timeout(function(){
                deferred.resolve(response.result);
            });
        }, function (error, status) {
            console.log("Invoke API error: "+error);
            deferred.reject(error);
        });
        return deferred.promise;
    };

    this.table = function(name){
        $rootScope.isLoading = true;
        return self.Client().getTable(name);
    };

}]);

/***********************************************************************************************************************************************************************/

tmApp.service("dataService", function(){
    //return field names 
    this.keys = function(data){
        return $.map(data, function(value, key) {
          return key;
        });
    };
});

/***********************************************************************************************************************************************************************/

tmApp.service("genericService", ['$location', '$timeout', function($location, $timeout){
    //Enum structure
    this.Enum = function (args) {
        for (var i in args) {
            this[args[i]] = i;
        }
    };

    //switch between views
    this.Redirect = function(redirectTo, delay){
        var timer = $timeout(function () {
            $timeout.cancel(timer);
            $location.path('/'+redirectTo);
        }, delay);
    };

}]);

/***********************************************************************************************************************************************************************/
tmApp.factory('cordova', function () {
  return {
      test: function(){
          document.addEventListener("deviceready", this.ready, false);
      },
      ready: function(){
          alert("Cordova ready!!");
      }

  }
})

//AuthService
/***********************************************************************************************************************************************************************/
tmApp.factory("authService", ['$rootScope', '$http', '$q', 'localStorageService', 'amsService', 'stringManipulator', function ($rootScope, $http, $q, localStorageService, amsService, stringManipulator) {

    var authServiceFactory = {};

    var _authentication = {
        isAuth: false,
        userName: ""
    };

    var _test = function(){
        alert("Test from authService.");
    };

    

    var _saveRegistration = function (data) {

        var deferred = $q.defer();

        _logOut();

        var tmp_gt = {};
        var _mRegistration = {};
        var payload = {};
        var promiseKey = amsService.invokeAPI("nacl", "get", null, tmp_gt);
        
        promiseKey.then(function(){

            _mRegistration.email = data.email;
            _mRegistration.pwd = stringManipulator.encrypt(data.pwd, tmp_gt.val);
            _mRegistration.firstName = data.firstName;
            _mRegistration.lastName = data.lastName;
            _mRegistration.nick = data.nick;
            _mRegistration.skillId = data.skillId;
            _mRegistration.playStyleId = data.playStyleId;
            _mRegistration.yearOfBirth = data.yearOfBirth;
            _mRegistration.gender = data.gender;
            _mRegistration.city = data.city;
            _mRegistration.countryState = data.countryState;
            _mRegistration.countryId = data.countryId;
            _mRegistration.created = new Date().toISOString();

            payload.nacl = tmp_gt.val;
            payload.lump = stringManipulator.encrypt(JSON.stringify(_mRegistration),tmp_gt.val);

            var pack = stringManipulator.pack(payload);
            amsService.invokeAPI("profile","post", pack)
            .then(function(response){
                deferred.resolve(response);
            }, function(error){
                deferred.reject(error.request.responseText);
            });

        });
        
        return deferred.promise;
    };


    var _login = function (loginData) {
        
        var deferred = $q.defer();

        var data = {
            grant_type: "user",
            email: loginData.userName,
            pwd: loginData.pwd
        };

        var promiseExists = _userExists(data.email);
        promiseExists.then(function(response){
            if(!response){
                deferred.reject("User does not exist.");
            }
            else{

                var payload = {};
                var tmp_gt = {};
                
                var promiseKey = amsService.invokeAPI("nacl?email="+data.email, "get", null, tmp_gt);
                
                promiseKey.then(function(){
                    payload.nacl = tmp_gt.val;
                    payload.lump = stringManipulator.encrypt(JSON.stringify(data),tmp_gt.val);

                    var pack = stringManipulator.pack(payload);
                    var access_token;

                    amsService.invokeAPI("tokenverify","post", pack, tmp_gt)
                    .then(function(response){
                        localStorageService.set('authorizationData', { token: access_token, userName: loginData.userName, userId: response });

                        _authentication.isAuth = true;
                        _authentication.userName = loginData.userName;

                        deferred.resolve(response);
                    }, function(error){
                        deferred.reject(error);
                    });
                });   
            }
        }, function(error){
            deferred.reject(error);
        });

        return deferred.promise;
    };

    var _logOut = function () {

        localStorageService.remove('authorizationData');

        _authentication.isAuth = false;
        _authentication.userName = "";

    };

    var _fillAuthData = function () {

        var authData = localStorageService.get('authorizationData');
        if (authData) {
            _authentication.isAuth = true;
            _authentication.userName = authData.userName;
        }

    };


    var _resetPassword = function(email){
        var deferred = $q.defer();
        var promiseExists = _userExists(email);
        promiseExists.then(function(response){
            if(!response){
                deferred.reject("User does not exist.");
            }
            else{
                var ret = {};
                var promiseReset = amsService.invokeAPI("profile/account/resetpwd", "post" , JSON.stringify(email), ret);
                promiseReset.then(function(response){
                    deferred.resolve(response);
                },function(error){
                    deferred.reject(error);
                });
            }
        });

        return deferred.promise;
    };

    var _userExists = function(email){
        var tmp = {};
        var deferred = $q.defer();
        var promiseUser = amsService.invokeAPI("profile/exists/email?value="+email, "get", null, tmp);
        promiseUser.then(function(response){
            deferred.resolve(response);
                
        }, function(error){
                deferred.reject(false);
            });

        return deferred.promise;
    };

    var _userId = function(){
        var authData = localStorageService.get('authorizationData');
        if(authData && authData.userId){
            return authData.userId;
        }
    }


    var _userDetails = function(){
        var deferred = $q.defer();

        if($rootScope.user_details && !angular.equals({}, $rootScope.user_details)){
            deferred.resolve($rootScope.user_details);
        }
        else{
            var id = _userId();
            var tmp = {};
            amsService.invokeAPI("profile/"+id, "get", null, tmp)
            .then(function(response){
                $rootScope.user_details = response;
                deferred.resolve($rootScope.user_details);
            },function(error){
                deferred.reject(error);
            });
        }
        
        return deferred.promise;
    }

    authServiceFactory.saveRegistration = _saveRegistration;
    authServiceFactory.login = _login;
    authServiceFactory.logOut = _logOut;
    authServiceFactory.fillAuthData = _fillAuthData;
    authServiceFactory.authentication = _authentication;
    authServiceFactory.resetPassword = _resetPassword;
    authServiceFactory.userExists = _userExists;
    authServiceFactory.userId = _userId;
    authServiceFactory.userDetails = _userDetails;

    authServiceFactory.test = _test;


    return authServiceFactory;
}]);

//AuthInterceptorService
/***********************************************************************************************************************************************************************/
tmApp.factory("authInterceptorService", ['$q', '$location', 'localStorageService', function ($q, $location, localStorageService) {

    var authInterceptorServiceFactory = {};

    var _request = function (config) {

        config.headers = config.headers || {};

        var authData = localStorageService.get('authorizationData');
        if (authData) {
            config.headers.Authorization = 'Bearer ' + authData.token;
        }
        else{
            $location.path('/login');
        }

        return config;
    }

    var _responseError = function (rejection) {
        if (rejection.status === 401) {
            $location.path('/login');
        }
        return $q.reject(rejection);
    }

    authInterceptorServiceFactory.request = _request;
    authInterceptorServiceFactory.responseError = _responseError;

    return authInterceptorServiceFactory;
}]);

//XHRInterceptorService
/***********************************************************************************************************************************************************************/
tmApp.factory("xhrInterceptorService", ['$http', function ($http) {

    var ajaxInterceptor = {};

    var COMPLETED_READY_STATE = 4;

    var RealXHRSend = XMLHttpRequest.prototype.send;

    var requestCallbacks = [];
    var responseCallbacks = [];

    var wired = false;

    var _wire = function() {
        if ( wired ) throw new Error("Ajax interceptor already wired");

        // Override send method of all XHR requests
        XMLHttpRequest.prototype.send = function() {

            // Fire request callbacks before sending the request
            fireCallbacks(requestCallbacks,this);

            // Wire response callbacks
            if( this.addEventListener ) {
                var self = this;
                this.addEventListener("readystatechange", function() {
                    fireResponseCallbacksIfCompleted(self);
                }, false);
            }
            else {
                proxifyOnReadyStateChange(this);
            }

            RealXHRSend.apply(this, arguments);
        };
        wired = true;
    };


    var _unwire = function() {
        if ( !wired ) throw new Error("Ajax interceptor not currently wired");
        XMLHttpRequest.prototype.send = RealXHRSend;
        wired = false;
    };

    var _addRequestCallback = function(callback) {
        requestCallbacks.push(callback);
    };

    var _removeRequestCallback = function(callback) {
        arrayRemove(requestCallbacks,callback);
    };


    var _addResponseCallback = function(callback) {
        responseCallbacks.push(callback);
    };

    var _removeResponseCallback = function(callback) {
        arrayRemove(responseCallbacks,callback);
    };

    function fireResponseCallbacksIfCompleted(xhr) {
        if( xhr.readyState === COMPLETED_READY_STATE ) {
            fireCallbacks(responseCallbacks,xhr);
        }
    }

    function proxifyOnReadyStateChange(xhr) {
        var realOnReadyStateChange = xhr.onreadystatechange;
        if ( realOnReadyStateChange ) {
            xhr.onreadystatechange = function() {
                fireResponseCallbacksIfCompleted(xhr);
                realOnReadyStateChange();
            };
        }
    }

    function arrayRemove(array,item) {
        var index = array.indexOf(item);
        if (index > -1) {
            array.splice(index, 1);
        } else {
            throw new Error("Could not remove " + item + " from array");
        }
    }

    function fireCallbacks(callbacks,xhr) {
        for( var i = 0; i < callbacks.length; i++ ) {
            callbacks[i](xhr);
        }
    }

    ajaxInterceptor.wire = _wire;
    ajaxInterceptor.unwire = _unwire;
    ajaxInterceptor.addRequestCallback = _addRequestCallback;
    ajaxInterceptor.removeRequestCallback = _removeRequestCallback;
    ajaxInterceptor.addResponseCallback = _addResponseCallback;
    ajaxInterceptor.removeResponseCallback = _removeResponseCallback;

    return ajaxInterceptor;

}]);

/***************************************************************************************************************************************************************/
tmApp.factory('stringManipulator', function() {
    
    var _encrypt = function(msg, nacl){
        var key = CryptoJS.enc.Utf8.parse(nacl);
        var iv = key;
        var tmp = msg;
        return CryptoJS.AES.encrypt(CryptoJS.enc.Utf8.parse(tmp), key,
        {
            keySize: 128 / 8,
            iv: iv,
            mode: CryptoJS.mode.CBC,
            padding: CryptoJS.pad.Pkcs7
        }).toString();
    };

    var _pack = function(data){
        var str = JSON.stringify(data);
        str = '"' + str.replace(/"/g, "'") + '"';

        return str;
    };

    var _toTitleCase = function (str)
    {
        return str.replace(/\w+/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
    };

    function _findInfo(result, type, longName) {
      for (var i = 0; i < result.address_components.length; i++) {
        var component = result.address_components[i];
        if (component.types.indexOf(type) !=-1) {
            if(longName){
                return component.long_name;
            }
            else{
                if(component.short_name.length <= 3){
                    return component.short_name;
                }
                else{
                    return "";
                }
            } 

        }
      }
      return false;
    };

    var uniqueArray = function(origArr) {
        var newArr = [],
        origLen = origArr.length,
        found, x, y;

        for (x = 0; x < origLen; x++) {
            found = undefined;
            for (y = 0; y < newArr.length; y++) {
                if (origArr[x] === newArr[y]) {
                    found = true;
                    break;
                }
            }
            if (!found) {
                newArr.push(origArr[x]);
            }
        }
        return newArr;
    };

    
    String.prototype.latinize=function(){
        var Latinise={
            latin_map : {"Á":"A","Ă":"A","Ắ":"A","Ặ":"A","Ằ":"A","Ẳ":"A","Ẵ":"A","Ǎ":"A","Â":"A","Ấ":"A","Ậ":"A","Ầ":"A","Ẩ":"A","Ẫ":"A","Ä":"A","Ǟ":"A","Ȧ":"A","Ǡ":"A","Ạ":"A","Ȁ":"A","À":"A","Ả":"A","Ȃ":"A","Ā":"A","Ą":"A","Å":"A","Ǻ":"A","Ḁ":"A","Ⱥ":"A","Ã":"A","Ꜳ":"AA","Æ":"AE","Ǽ":"AE","Ǣ":"AE","Ꜵ":"AO","Ꜷ":"AU","Ꜹ":"AV","Ꜻ":"AV","Ꜽ":"AY","Ḃ":"B","Ḅ":"B","Ɓ":"B","Ḇ":"B","Ƀ":"B","Ƃ":"B","Ć":"C","Č":"C","Ç":"C","Ḉ":"C","Ĉ":"C","Ċ":"C","Ƈ":"C","Ȼ":"C","Ď":"D","Ḑ":"D","Ḓ":"D","Ḋ":"D","Ḍ":"D","Ɗ":"D","Ḏ":"D","ǲ":"D","ǅ":"D","Đ":"D","Ƌ":"D","Ǳ":"DZ","Ǆ":"DZ","É":"E","Ĕ":"E","Ě":"E","Ȩ":"E","Ḝ":"E","Ê":"E","Ế":"E","Ệ":"E","Ề":"E","Ể":"E","Ễ":"E","Ḙ":"E","Ë":"E","Ė":"E","Ẹ":"E","Ȅ":"E","È":"E","Ẻ":"E","Ȇ":"E","Ē":"E","Ḗ":"E","Ḕ":"E","Ę":"E","Ɇ":"E","Ẽ":"E","Ḛ":"E","Ꝫ":"ET","Ḟ":"F","Ƒ":"F","Ǵ":"G","Ğ":"G","Ǧ":"G","Ģ":"G","Ĝ":"G","Ġ":"G","Ɠ":"G","Ḡ":"G","Ǥ":"G","Ḫ":"H","Ȟ":"H","Ḩ":"H","Ĥ":"H","Ⱨ":"H","Ḧ":"H","Ḣ":"H","Ḥ":"H","Ħ":"H","Í":"I","Ĭ":"I","Ǐ":"I","Î":"I","Ï":"I","Ḯ":"I","İ":"I","Ị":"I","Ȉ":"I","Ì":"I","Ỉ":"I","Ȋ":"I","Ī":"I","Į":"I","Ɨ":"I","Ĩ":"I","Ḭ":"I","Ꝺ":"D","Ꝼ":"F","Ᵹ":"G","Ꞃ":"R","Ꞅ":"S","Ꞇ":"T","Ꝭ":"IS","Ĵ":"J","Ɉ":"J","Ḱ":"K","Ǩ":"K","Ķ":"K","Ⱪ":"K","Ꝃ":"K","Ḳ":"K","Ƙ":"K","Ḵ":"K","Ꝁ":"K","Ꝅ":"K","Ĺ":"L","Ƚ":"L","Ľ":"L","Ļ":"L","Ḽ":"L","Ḷ":"L","Ḹ":"L","Ⱡ":"L","Ꝉ":"L","Ḻ":"L","Ŀ":"L","Ɫ":"L","ǈ":"L","Ł":"L","Ǉ":"LJ","Ḿ":"M","Ṁ":"M","Ṃ":"M","Ɱ":"M","Ń":"N","Ň":"N","Ņ":"N","Ṋ":"N","Ṅ":"N","Ṇ":"N","Ǹ":"N","Ɲ":"N","Ṉ":"N","Ƞ":"N","ǋ":"N","Ñ":"N","Ǌ":"NJ","Ó":"O","Ŏ":"O","Ǒ":"O","Ô":"O","Ố":"O","Ộ":"O","Ồ":"O","Ổ":"O","Ỗ":"O","Ö":"O","Ȫ":"O","Ȯ":"O","Ȱ":"O","Ọ":"O","Ő":"O","Ȍ":"O","Ò":"O","Ỏ":"O","Ơ":"O","Ớ":"O","Ợ":"O","Ờ":"O","Ở":"O","Ỡ":"O","Ȏ":"O","Ꝋ":"O","Ꝍ":"O","Ō":"O","Ṓ":"O","Ṑ":"O","Ɵ":"O","Ǫ":"O","Ǭ":"O","Ø":"O","Ǿ":"O","Õ":"O","Ṍ":"O","Ṏ":"O","Ȭ":"O","Ƣ":"OI","Ꝏ":"OO","Ɛ":"E","Ɔ":"O","Ȣ":"OU","Ṕ":"P","Ṗ":"P","Ꝓ":"P","Ƥ":"P","Ꝕ":"P","Ᵽ":"P","Ꝑ":"P","Ꝙ":"Q","Ꝗ":"Q","Ŕ":"R","Ř":"R","Ŗ":"R","Ṙ":"R","Ṛ":"R","Ṝ":"R","Ȑ":"R","Ȓ":"R","Ṟ":"R","Ɍ":"R","Ɽ":"R","Ꜿ":"C","Ǝ":"E","Ś":"S","Ṥ":"S","Š":"S","Ṧ":"S","Ş":"S","Ŝ":"S","Ș":"S","Ṡ":"S","Ṣ":"S","Ṩ":"S","Ť":"T","Ţ":"T","Ṱ":"T","Ț":"T","Ⱦ":"T","Ṫ":"T","Ṭ":"T","Ƭ":"T","Ṯ":"T","Ʈ":"T","Ŧ":"T","Ɐ":"A","Ꞁ":"L","Ɯ":"M","Ʌ":"V","Ꜩ":"TZ","Ú":"U","Ŭ":"U","Ǔ":"U","Û":"U","Ṷ":"U","Ü":"U","Ǘ":"U","Ǚ":"U","Ǜ":"U","Ǖ":"U","Ṳ":"U","Ụ":"U","Ű":"U","Ȕ":"U","Ù":"U","Ủ":"U","Ư":"U","Ứ":"U","Ự":"U","Ừ":"U","Ử":"U","Ữ":"U","Ȗ":"U","Ū":"U","Ṻ":"U","Ų":"U","Ů":"U","Ũ":"U","Ṹ":"U","Ṵ":"U","Ꝟ":"V","Ṿ":"V","Ʋ":"V","Ṽ":"V","Ꝡ":"VY","Ẃ":"W","Ŵ":"W","Ẅ":"W","Ẇ":"W","Ẉ":"W","Ẁ":"W","Ⱳ":"W","Ẍ":"X","Ẋ":"X","Ý":"Y","Ŷ":"Y","Ÿ":"Y","Ẏ":"Y","Ỵ":"Y","Ỳ":"Y","Ƴ":"Y","Ỷ":"Y","Ỿ":"Y","Ȳ":"Y","Ɏ":"Y","Ỹ":"Y","Ź":"Z","Ž":"Z","Ẑ":"Z","Ⱬ":"Z","Ż":"Z","Ẓ":"Z","Ȥ":"Z","Ẕ":"Z","Ƶ":"Z","Ĳ":"IJ","Œ":"OE","ᴀ":"A","ᴁ":"AE","ʙ":"B","ᴃ":"B","ᴄ":"C","ᴅ":"D","ᴇ":"E","ꜰ":"F","ɢ":"G","ʛ":"G","ʜ":"H","ɪ":"I","ʁ":"R","ᴊ":"J","ᴋ":"K","ʟ":"L","ᴌ":"L","ᴍ":"M","ɴ":"N","ᴏ":"O","ɶ":"OE","ᴐ":"O","ᴕ":"OU","ᴘ":"P","ʀ":"R","ᴎ":"N","ᴙ":"R","ꜱ":"S","ᴛ":"T","ⱻ":"E","ᴚ":"R","ᴜ":"U","ᴠ":"V","ᴡ":"W","ʏ":"Y","ᴢ":"Z","á":"a","ă":"a","ắ":"a","ặ":"a","ằ":"a","ẳ":"a","ẵ":"a","ǎ":"a","â":"a","ấ":"a","ậ":"a","ầ":"a","ẩ":"a","ẫ":"a","ä":"a","ǟ":"a","ȧ":"a","ǡ":"a","ạ":"a","ȁ":"a","à":"a","ả":"a","ȃ":"a","ā":"a","ą":"a","ᶏ":"a","ẚ":"a","å":"a","ǻ":"a","ḁ":"a","ⱥ":"a","ã":"a","ꜳ":"aa","æ":"ae","ǽ":"ae","ǣ":"ae","ꜵ":"ao","ꜷ":"au","ꜹ":"av","ꜻ":"av","ꜽ":"ay","ḃ":"b","ḅ":"b","ɓ":"b","ḇ":"b","ᵬ":"b","ᶀ":"b","ƀ":"b","ƃ":"b","ɵ":"o","ć":"c","č":"c","ç":"c","ḉ":"c","ĉ":"c","ɕ":"c","ċ":"c","ƈ":"c","ȼ":"c","ď":"d","ḑ":"d","ḓ":"d","ȡ":"d","ḋ":"d","ḍ":"d","ɗ":"d","ᶑ":"d","ḏ":"d","ᵭ":"d","ᶁ":"d","đ":"d","ɖ":"d","ƌ":"d","ı":"i","ȷ":"j","ɟ":"j","ʄ":"j","ǳ":"dz","ǆ":"dz","é":"e","ĕ":"e","ě":"e","ȩ":"e","ḝ":"e","ê":"e","ế":"e","ệ":"e","ề":"e","ể":"e","ễ":"e","ḙ":"e","ë":"e","ė":"e","ẹ":"e","ȅ":"e","è":"e","ẻ":"e","ȇ":"e","ē":"e","ḗ":"e","ḕ":"e","ⱸ":"e","ę":"e","ᶒ":"e","ɇ":"e","ẽ":"e","ḛ":"e","ꝫ":"et","ḟ":"f","ƒ":"f","ᵮ":"f","ᶂ":"f","ǵ":"g","ğ":"g","ǧ":"g","ģ":"g","ĝ":"g","ġ":"g","ɠ":"g","ḡ":"g","ᶃ":"g","ǥ":"g","ḫ":"h","ȟ":"h","ḩ":"h","ĥ":"h","ⱨ":"h","ḧ":"h","ḣ":"h","ḥ":"h","ɦ":"h","ẖ":"h","ħ":"h","ƕ":"hv","í":"i","ĭ":"i","ǐ":"i","î":"i","ï":"i","ḯ":"i","ị":"i","ȉ":"i","ì":"i","ỉ":"i","ȋ":"i","ī":"i","į":"i","ᶖ":"i","ɨ":"i","ĩ":"i","ḭ":"i","ꝺ":"d","ꝼ":"f","ᵹ":"g","ꞃ":"r","ꞅ":"s","ꞇ":"t","ꝭ":"is","ǰ":"j","ĵ":"j","ʝ":"j","ɉ":"j","ḱ":"k","ǩ":"k","ķ":"k","ⱪ":"k","ꝃ":"k","ḳ":"k","ƙ":"k","ḵ":"k","ᶄ":"k","ꝁ":"k","ꝅ":"k","ĺ":"l","ƚ":"l","ɬ":"l","ľ":"l","ļ":"l","ḽ":"l","ȴ":"l","ḷ":"l","ḹ":"l","ⱡ":"l","ꝉ":"l","ḻ":"l","ŀ":"l","ɫ":"l","ᶅ":"l","ɭ":"l","ł":"l","ǉ":"lj","ſ":"s","ẜ":"s","ẛ":"s","ẝ":"s","ḿ":"m","ṁ":"m","ṃ":"m","ɱ":"m","ᵯ":"m","ᶆ":"m","ń":"n","ň":"n","ņ":"n","ṋ":"n","ȵ":"n","ṅ":"n","ṇ":"n","ǹ":"n","ɲ":"n","ṉ":"n","ƞ":"n","ᵰ":"n","ᶇ":"n","ɳ":"n","ñ":"n","ǌ":"nj","ó":"o","ŏ":"o","ǒ":"o","ô":"o","ố":"o","ộ":"o","ồ":"o","ổ":"o","ỗ":"o","ö":"o","ȫ":"o","ȯ":"o","ȱ":"o","ọ":"o","ő":"o","ȍ":"o","ò":"o","ỏ":"o","ơ":"o","ớ":"o","ợ":"o","ờ":"o","ở":"o","ỡ":"o","ȏ":"o","ꝋ":"o","ꝍ":"o","ⱺ":"o","ō":"o","ṓ":"o","ṑ":"o","ǫ":"o","ǭ":"o","ø":"o","ǿ":"o","õ":"o","ṍ":"o","ṏ":"o","ȭ":"o","ƣ":"oi","ꝏ":"oo","ɛ":"e","ᶓ":"e","ɔ":"o","ᶗ":"o","ȣ":"ou","ṕ":"p","ṗ":"p","ꝓ":"p","ƥ":"p","ᵱ":"p","ᶈ":"p","ꝕ":"p","ᵽ":"p","ꝑ":"p","ꝙ":"q","ʠ":"q","ɋ":"q","ꝗ":"q","ŕ":"r","ř":"r","ŗ":"r","ṙ":"r","ṛ":"r","ṝ":"r","ȑ":"r","ɾ":"r","ᵳ":"r","ȓ":"r","ṟ":"r","ɼ":"r","ᵲ":"r","ᶉ":"r","ɍ":"r","ɽ":"r","ↄ":"c","ꜿ":"c","ɘ":"e","ɿ":"r","ś":"s","ṥ":"s","š":"s","ṧ":"s","ş":"s","ŝ":"s","ș":"s","ṡ":"s","ṣ":"s","ṩ":"s","ʂ":"s","ᵴ":"s","ᶊ":"s","ȿ":"s","ɡ":"g","ᴑ":"o","ᴓ":"o","ᴝ":"u","ť":"t","ţ":"t","ṱ":"t","ț":"t","ȶ":"t","ẗ":"t","ⱦ":"t","ṫ":"t","ṭ":"t","ƭ":"t","ṯ":"t","ᵵ":"t","ƫ":"t","ʈ":"t","ŧ":"t","ᵺ":"th","ɐ":"a","ᴂ":"ae","ǝ":"e","ᵷ":"g","ɥ":"h","ʮ":"h","ʯ":"h","ᴉ":"i","ʞ":"k","ꞁ":"l","ɯ":"m","ɰ":"m","ᴔ":"oe","ɹ":"r","ɻ":"r","ɺ":"r","ⱹ":"r","ʇ":"t","ʌ":"v","ʍ":"w","ʎ":"y","ꜩ":"tz","ú":"u","ŭ":"u","ǔ":"u","û":"u","ṷ":"u","ü":"u","ǘ":"u","ǚ":"u","ǜ":"u","ǖ":"u","ṳ":"u","ụ":"u","ű":"u","ȕ":"u","ù":"u","ủ":"u","ư":"u","ứ":"u","ự":"u","ừ":"u","ử":"u","ữ":"u","ȗ":"u","ū":"u","ṻ":"u","ų":"u","ᶙ":"u","ů":"u","ũ":"u","ṹ":"u","ṵ":"u","ᵫ":"ue","ꝸ":"um","ⱴ":"v","ꝟ":"v","ṿ":"v","ʋ":"v","ᶌ":"v","ⱱ":"v","ṽ":"v","ꝡ":"vy","ẃ":"w","ŵ":"w","ẅ":"w","ẇ":"w","ẉ":"w","ẁ":"w","ⱳ":"w","ẘ":"w","ẍ":"x","ẋ":"x","ᶍ":"x","ý":"y","ŷ":"y","ÿ":"y","ẏ":"y","ỵ":"y","ỳ":"y","ƴ":"y","ỷ":"y","ỿ":"y","ȳ":"y","ẙ":"y","ɏ":"y","ỹ":"y","ź":"z","ž":"z","ẑ":"z","ʑ":"z","ⱬ":"z","ż":"z","ẓ":"z","ȥ":"z","ẕ":"z","ᵶ":"z","ᶎ":"z","ʐ":"z","ƶ":"z","ɀ":"z","ﬀ":"ff","ﬃ":"ffi","ﬄ":"ffl","ﬁ":"fi","ﬂ":"fl","ĳ":"ij","œ":"oe","ﬆ":"st","ₐ":"a","ₑ":"e","ᵢ":"i","ⱼ":"j","ₒ":"o","ᵣ":"r","ᵤ":"u","ᵥ":"v","ₓ":"x"}
        };
        return this.replace(/[^A-Za-z0-9\[\] ]/g,function(a){return Latinise.latin_map[a]||a})
    };

    var retVal = {};

    retVal.encrypt = _encrypt;
    retVal.pack = _pack;
    retVal.toTitleCase = _toTitleCase;

    return retVal;
});
/***************************************************************************************************************************************************************/
tmApp.factory('gmService', ['$window', '$q','$http', '$rootScope','$timeout',  function($window, $q, $http, $rootScope, $timeout){

    var key = "AIzaSyAB4dLVEn18yBJA4X1A-215BneAWnEXdKM";

    var _init = function(){
        var mapsDefer = $q.defer();
        console.log("getting device position....");
        //get position from device
        if (navigator.geolocation) {
            var location_timeout = setTimeout("geolocFail()", 10000);
            navigator.geolocation.getCurrentPosition(function(position){
                clearTimeout(location_timeout);
                var loc = {
                    lat: position.coords.latitude,
                    lng: position.coords.longitude
                };
                $rootScope.gmMyLocation = new google.maps.LatLng(loc.lat, loc.lng);
                var gmMap = new google.maps.Map(document.createElement('div'), {
                  center: $rootScope.gmMyLocation,
                  zoom: 15
                });
                $rootScope.gmPlaceService = new google.maps.places.PlacesService(gmMap);
                $rootScope.gmGeocoder = new google.maps.Geocoder();
                console.log("device position found....");
                $timeout(function(){
                    mapsDefer.resolve;
                });
            }, function(error){
                clearTimeout(location_timeout);
                console.log("geolocation failed.");
                mapsDefer.reject("Error in geolocation: "+error);
            });
        }
        else{
            console.log("geolocation disabled.");
            mapsDefer.reject("Could not initiate geolocation");
        }

        var geolocFail = function(){
            alert("geolocFail !!");
            
        };

        return mapsDefer.promise;
    };

    var _placeDetails = function(argPlaceId){
        if(!argPlaceId || argPlaceId.length == 0){
            return;
        }

        var mapsDefer = $q.defer();
        var request = {
            placeId: argPlaceId
        };

        $rootScope.gmPlaceService.getDetails(request, function(place, status){
            if (status == google.maps.places.PlacesServiceStatus.OK) {
                
                var retVal = _addressParts(place.address_components);
                var check = "";

                check = place.international_phone_number;
                if(check && check.length > 0){
                    retVal.phone = check;
                }

                check = place.website;
                if(check && check.length > 0){
                    retVal.website = check;
                }

                check = place.url;
                if(check && check.length > 0){
                    retVal.url = check;
                }

                mapsDefer.resolve(retVal);
            }
            else{
                mapsDefer.reject("Could not contact Google Place details service.");
            }
        });

        return mapsDefer.promise;
    };

    var _tennisClubs = function(){    
        var request = {
            location: $rootScope.gmMyLocation,
            radius: '10000',
            query: 'tennis+club'
        };
        var mapsDefer = $q.defer();
        $rootScope.isLoading = true;
        if(!$rootScope.gmPlaceService){
            console.log("Places service not ready!");
            return;
        }
        $rootScope.gmPlaceService.textSearch(request, function(results, status){
            if (status == google.maps.places.PlacesServiceStatus.OK) {
                $rootScope.isLoading = false;
                $timeout(function(){
                    mapsDefer.resolve(results);
                });
            }
            else{
                $rootScope.isLoading = false;
                mapsDefer.reject("Could not contact Google Places service.");
                alert("Places service text search error");
            }
        }, function(error){
            alert("Places service text search error: "+error);
        });        

        return mapsDefer.promise;
    };


    var _addressParts = function(gmAddressComponents){
        var retVal = {
                    street_number: "",
                    street: "",
                    postal_code: "",
                    city:"",
                    state:"",
                    country_long:"",
                    country_short:"",
                    phone:"",
                    website:"",
                    url:""
                };

        var check = "";

        check = jlinq.from(gmAddressComponents)
                    .contains("types", "street_number")
                    .select();
        if(check && check.length > 0){
            retVal.street_number = check[0].long_name;
        }

        check = jlinq.from(gmAddressComponents)
                    .contains("types", "route")
                    .select();
        if(check && check.length > 0){
            retVal.street = check[0].long_name;
        }

        check = jlinq.from(gmAddressComponents)
                    .contains("types", "locality")
                    .select();
        if(!check || check.length==0){
            check = jlinq.from(gmAddressComponents)
                        .contains("types", "sublocality_level_1")
                        .select();
            if(!check || check.length==0){
                check = jlinq.from(gmAddressComponents)
                        .contains("types", "sublocality")
                        .select();
                if(check && check.length>0){
                    retVal.city = check[0].long_name;
                }
            }
            else{
                retVal.city = check[0].long_name;
            }
        }
        else{
            retVal.city = check[0].long_name;
        }

        check = jlinq.from(gmAddressComponents)
                    .contains("types" ,"postal_code")
                    .select();
        if(check && check.length > 0){
            retVal.postal_code = check[0].long_name;
        }

        check = jlinq.from(gmAddressComponents)
                    .contains("types" ,"administrative_area_level_1")
                    .select();
        if(check && check.length > 0){
            retVal.state = check[0].long_name;
        }

        check = jlinq.from(gmAddressComponents)
                    .contains("types" ,"country")
                    .select();
        if(check && check.length > 0){
            retVal.country_long = check[0].long_name;
            retVal.country_short = check[0].short_name;
        }

        return retVal;

    };

    var retVal = {};

    retVal.init = _init;
    retVal.tennisClubs = _tennisClubs;
    retVal.placeDetails = _placeDetails;
    retVal.addressParts = _addressParts;

    return retVal;
}]);