//Directive Code Goes here
'use strict';
/***************************************************************************************************************************************************************************/
//TENNIS MATE app
/*****************************************************************************************************************************************************************/
tmApp.directive("tmSlideMenu", function(){
  //bind the JavaScript events to the scope.
  var linkFunction = function( scope, element, attributes ) {
      console.log( "tmSlide menu directive linking..." );
  };

  return {
      restrict: "A",
      templateUrl: "../app/views/slide-menu.html",
      link: linkFunction
    };
});

/*****************************************************************************************************************************************************************/
tmApp.directive("tmHeader", function(){
  //bind the JavaScript events to the scope.
  var linkFunction = function( scope, element, attributes ) {
      console.log( "tmHeader directive linking..." );
  };

  return {
      restrict: "EA",
      //templateUrl: "../app/views/templates/header.html",
     //replace:true,
      link: linkFunction
    };
});

/*****************************************************************************************************************************************************************/
tmApp.directive("tmFooter", function(){
  //bind the JavaScript events to the scope.
  var linkFunction = function( scope, element, attributes ) {
      console.log( "tmFooter directive linking..." );
  };

  return {
      restrict: "EA",
      templateUrl: "../app/views/templates/footer.html",
      replace:true,
      link: linkFunction
    };
});

/*****************************************************************************************************************************************************************/
tmApp.directive("tmLoginHeader", ['$state', '$ionicTemplateLoader','$ionicBackdrop','$ionicPlatform',
                                  '$http',
  function($state, $ionicTemplateLoader, $ionicBackdrop, $ionicPlatform, $http){
  //bind the JavaScript events to the scope.
  var linkFunction = function( scope, element, attrs ) {
      console.log( "tmLoginHeader directive linking..." );

      var type="";
      type = attrs.type;

      if(type.length > 0){
        if(type.toLowerCase()==="login"){
          $("#link-left",element).on("click", function(e){
            $state.go("forgotpassword");
          });

          $("#link-right",element).on("click", function(e){
            $state.go("signup");
          });
        }
        else if(type.toLowerCase()==="signup"){
          $("#link-left",element).on("click", function(e){
            $state.go("forgotpassword");
          });

          $("#link-right",element).on("click", function(e){
            $state.go("login");
          });
        }
        else if(type.toLowerCase()==="resetpwd"){
          $("#link-left",element).on("click", function(e){
            $state.go("signup");
          });
          
          $("#link-right",element).on("click", function(e){
            $state.go("login");
          });
        }
      }
  };

  return {
      restrict: "EA",
      templateUrl: "../app/views/templates/login/header.html",
      replace: true,
      link: linkFunction
    };
}]);

/*****************************************************************************************************************************************************************/
tmApp.directive("tmLoginFooter", function(){
  
   var type="";

  return {
      restrict: "EA",
      templateUrl: function(elm, attrs){
        type = attrs.type;

        if(type.length > 0){
          if(type.toLowerCase()==="login" || type.toLowerCase()==="resetpwd"){
            return "../app/views/templates/login/footer.html";
          }
          else if(type.toLowerCase()==="signup"){
            return "../app/views/templates/login/footer-signup.html";
          }
        }
      },
      replace:true
    };
});

/*****************************************************************************************************************************************************************/
tmApp.directive('focus', function($timeout) {
  return {
    scope : { trigger : '@focus' },
    link : function(scope, element) {
      scope.$watch('trigger', function(value){
        //console.log("newValue: "+value);
        if (scope.$eval(value)) {
            $timeout(function() {
              element[0].focus();
            });       
        }
      });
    }

  };

});

/*****************************************************************************************************************************************************************/
tmApp.directive('passwordToggle',function($compile, $timeout, $rootScope){
    
    function setPosition(scope,pos,elm)
    {
        setTimeout(function(){
          $(elm).css({"left":pos});
          $compile(elm)(scope);
        },50);
    }

    return {
        restrict: 'A',
        scope:{},
        link: function(scope,elem,attrs){
            
            var typeFlag="password";
            var glyph = "<i class=\""+(typeFlag==="text" ? "fa fa-eye-slash" : "fa fa-eye")+"\"></i>";

            scope.tgl = function(){ 
              elem.attr('type',(elem.attr('type')==='text'?'password':'text'));
              typeFlag = elem.attr('type');
              glyph = "<i class=\""+(typeFlag==="text" ? "fa fa-eye-slash" : "fa fa-eye")+"\"></i>";
              $(lnk).html(glyph);
            };
            
            //element itself
            var lnk = angular.element('<a class="hidden-xs" data-ng-mousedown="tgl()" data-ng-mouseup="tgl()">'+glyph+'</a>');
            //position
            var pos = (elem[0].getBoundingClientRect().width) - ($(lnk).width()+45);
            //parent input element
            var input = $(elem).closest("input");
            elem.wrap('<div class="password-toggle"/>').after(lnk);
            
            //react on resize of element
            scope.getElementDimensions = function () {
              return { 'h': $(input).height(), 'w': $(input).width(), 'l':$(input).left };
            };

            scope.$watch(scope.getElementDimensions, function (newValue) {
              pos = (scope.getElementDimensions.w+scope.getElementDimensions.l) - ($(lnk).width());
            }, true);

            $(input).bind('resize', function () {
              scope.$apply();
            });

              //react on orientation change
              $rootScope.$watch('orientationPortrait', function(newValue) {
                if($rootScope.orientationPortrait){
                  pos = (elem[0].getBoundingClientRect().width) - ($(lnk).width()+12);
                  console.log("Orientation change: portrait");
                }
                else{
                  pos = (elem[0].getBoundingClientRect().width+elem[0].getBoundingClientRect().left) - ($(lnk).width()+elem[0].getBoundingClientRect().left+12);
                  console.log("Orientation change: landscape");
                }
                setPosition(scope,pos,lnk);
              },true);
        }
    }
});

/*****************************************************************************************************************************************************************/
tmApp.directive('stringToNumber', function() {
  return {
    require: 'ngModel',
    link: function(scope, element, attrs, ngModel) {
      ngModel.$parsers.push(function(value) {
        return '' + value;
      });
      ngModel.$formatters.push(function(value) {
        return parseFloat(value, 10);
      });
    }
  };
});

/*****************************************************************************************************************************************************************/
tmApp.directive('faIcon',['$parse', '$timeout', function($parse, $timeout) {
  var compileFunction = function (scope, $element, attributes) {
    var icon = attributes.faIcon;

    $timeout(function(){
      $($element).find("span.caret").html("<i class=\"fa "+icon+"\"></i>").removeClass("caret").addClass("fa-dropdown-addon").attr("style","");
    });

  }

  return {
    restrict: 'A',
    link: compileFunction
  };

}]);

/*****************************************************************************************************************************************************************/
tmApp.directive('eatClickIf', ['$parse', '$rootScope', function($parse, $rootScope) {
  return {
    priority: 100,
    restrict: 'A',
    compile: function($element, attr) {
      var fn = $parse(attr.eatClickIf);
      return {
        pre: function link(scope, element) {
          var eventName = 'click';
          element.on(eventName, function(event) {
            var callback = function() {
              if (fn(scope, {$event: event})) {
                event.stopImmediatePropagation();
                event.preventDefault();
                return false;
              }
            };
            if ($rootScope.$$phase) {
              scope.$evalAsync(callback);
            } else {
              scope.$apply(callback);
            }
          });
        },
        post: function() {}
      }
    }
  };
}]);

/**************************************************************************************************************************************************************/
tmApp.directive('charLimit', ['$parse', function($parse) {
  var linkFunction = function (scope, element, attr) {
    var self = $(element);
    if($(self).prop("tagName").toLowerCase()!=="textarea"){
      return false;
    }

    var chars = 0;
    chars = attr.charLimit;

    if(chars == 0){
      chars = 512;
    }

    $("#charCounter").html("Characters left: "+chars-self.length);

    $(self).keydown(function(e){
        var Length = $(this).val().length;
        var AmountLeft = 0;
        if(e.keyCode === 8){
          AmountLeft = chars - (Length - 1);
        }
        else{
          AmountLeft = chars - (Length + 1);
        }
        if(AmountLeft > chars){
          AmountLeft = chars;
        }
       $("#charCounter").html("Characters left: "+AmountLeft);

       if(Length >= chars && e.keyCode != 8){
          e.preventDefault(); // will cancel the default action of the event
       }
    });

  };

  return {
    restrict: 'A',
    link: linkFunction
  };

}]);

/**************************************************************************************************************************************************************/
tmApp.directive('pageslide', ['$document', function ($document) {
    var defaults = {};

    return {
        restrict: 'EAC',
        transclude: false,
        scope: {
            psOpen: '=?',
            psAutoClose: '=?',
            psSide: '@',
            psSpeed: '@',
            psClass: '@',
            psSize: '@',
            psSqueeze: '@',
            psCloak: '@',
            psPush: '@',
            psContainer: '@',
            psKeyListener: '@',
            psBodyClass: '@'
        },
        link: function ($scope, el, attrs) {
            /* Inspect */

            //console.log($scope);
            //console.log(el);
            //console.log(attrs);

            /* Parameters */
            var param = {};

            param.side = $scope.psSide || 'right';
            param.speed = $scope.psSpeed || '0.5';
            param.size = $scope.psSize || '300px';
            param.zindex = 1000; // Override with custom CSS
            param.className = $scope.psClass || 'ng-pageslide';
            param.cloak = $scope.psCloak && $scope.psCloak.toLowerCase() == 'false' ? false : true;
            param.squeeze = Boolean($scope.psSqueeze) || false;
            param.push = Boolean($scope.psPush) || false;
            param.container = $scope.psContainer || false;
            param.keyListener = Boolean($scope.psKeyListener) || false;
            param.bodyClass = $scope.psBodyClass || false;

            // Apply Class
            el.addClass(param.className);

            /* DOM manipulation */
            var content = null;
            var slider = null;
            var body = param.container ? document.getElementById(param.container) : document.body;

            function setBodyClass(value){
                if (param.bodyClass) {
                    var bodyClass = param.className + '-body';
                    var bodyClassRe = new RegExp(' ' + bodyClass + '-closed| ' + bodyClass + '-open');
                    body.className = body.className.replace(bodyClassRe, '');
                    body.className += ' ' + bodyClass + '-' + value;
                }
            }

            setBodyClass('closed');

            slider = el[0];

            // Check for div tag
            if (slider.tagName.toLowerCase() !== 'div' &&
            slider.tagName.toLowerCase() !== 'pageslide')
            throw new Error('Pageslide can only be applied to <div> or <pageslide> elements');

            // Check for content
            if (slider.children.length === 0)
                throw new Error('You have to content inside the <pageslide>');

            content = angular.element(slider.children);

            /* Append */
            body.appendChild(slider);

            /* Style setup */
            slider.style.zIndex = param.zindex;
            slider.style.position = param.container !== false ? 'absolute' : 'fixed';
            slider.style.width = 0;
            slider.style.height = 0;
            slider.style.overflow = 'hidden';
            slider.style.transitionDuration = param.speed + 's';
            slider.style.webkitTransitionDuration = param.speed + 's';
            slider.style.transitionProperty = 'width, height';

            if (param.squeeze) {
                body.style.position = 'absolute';
                body.style.transitionDuration = param.speed + 's';
                body.style.webkitTransitionDuration = param.speed + 's';
                body.style.transitionProperty = 'top, bottom, left, right';
            }

            switch (param.side) {
                case 'right':
                    slider.style.height = attrs.psCustomHeight || '100%';
                    slider.style.top = attrs.psCustomTop || '0px';
                    slider.style.bottom = attrs.psCustomBottom || '0px';
                    slider.style.right = attrs.psCustomRight || '0px';
                    break;
                case 'left':
                    slider.style.height = attrs.psCustomHeight || '100%';
                    slider.style.top = attrs.psCustomTop || '0px';
                    slider.style.bottom = attrs.psCustomBottom || '0px';
                    slider.style.left = attrs.psCustomLeft || '0px';
                    break;
                case 'top':
                    slider.style.width = attrs.psCustomWidth || '100%';
                    slider.style.left = attrs.psCustomLeft || '0px';
                    slider.style.top = attrs.psCustomTop || '0px';
                    slider.style.right = attrs.psCustomRight || '0px';
                    break;
                case 'bottom':
                    slider.style.width = attrs.psCustomWidth || '100%';
                    slider.style.bottom = attrs.psCustomBottom || '0px';
                    slider.style.left = attrs.psCustomLeft || '0px';
                    slider.style.right = attrs.psCustomRight || '0px';
                    break;
            }


            /* Closed */
            function psClose(slider, param) {
                if (slider && slider.style.width !== 0 && slider.style.width !== 0) {
                    if (param.cloak) content.css('display', 'none');
                    switch (param.side) {
                        case 'right':
                            slider.style.width = '0px';
                            if (param.squeeze) body.style.right = '0px';
                            if (param.push) {
                                body.style.right = '0px';
                                body.style.left = '0px';
                            }
                            break;
                        case 'left':
                            slider.style.width = '0px';
                            if (param.squeeze) body.style.left = '0px';
                            if (param.push) {
                                body.style.left = '0px';
                                body.style.right = '0px';
                            }
                            break;
                        case 'top':
                            slider.style.height = '0px';
                            if (param.squeeze) body.style.top = '0px';
                            if (param.push) {
                                body.style.top = '0px';
                                body.style.bottom = '0px';
                            }
                            break;
                        case 'bottom':
                            slider.style.height = '0px';
                            if (param.squeeze) body.style.bottom = '0px';
                            if (param.push) {
                                body.style.bottom = '0px';
                                body.style.top = '0px';
                            }
                            break;
                    }
                }
                $scope.psOpen = false;

                if (param.keyListener) {
                    $document.off('keydown', keyListener);
                }

                setBodyClass('closed');
            }

            /* Open */
            function psOpen(slider, param) {
                if (slider.style.width !== 0 && slider.style.width !== 0) {
                    switch (param.side) {
                        case 'right':
                            slider.style.width = param.size;
                            if (param.squeeze) body.style.right = param.size;
                            if (param.push) {
                                body.style.right = param.size;
                                body.style.left = '-' + param.size;
                            }
                            break;
                        case 'left':
                            slider.style.width = param.size;
                            if (param.squeeze) body.style.left = param.size;
                            if (param.push) {
                                body.style.left = param.size;
                                body.style.right = '-' + param.size;
                            }
                            break;
                        case 'top':
                            slider.style.height = param.size;
                            if (param.squeeze) body.style.top = param.size;
                            if (param.push) {
                                body.style.top = param.size;
                                body.style.bottom = '-' + param.size;
                            }
                            break;
                        case 'bottom':
                            slider.style.height = param.size;
                            if (param.squeeze) body.style.bottom = param.size;
                            if (param.push) {
                                body.style.bottom = param.size;
                                body.style.top = '-' + param.size;
                            }
                            break;
                    }
                    setTimeout(function() {
                        if (param.cloak) content.css('display', 'block');
                    }, (param.speed * 1000));

                    if (param.keyListener) {
                        $document.on('keydown', keyListener);
                    }

                    setBodyClass('open');
                }
            }

            function isFunction(functionToCheck) {
                var getType = {};
                return functionToCheck && getType.toString.call(functionToCheck) === '[object Function]';
            }

            /**
            * Close the sidebar if the 'esc' key is pressed
            */
            function keyListener(e) {
                var ESC_KEY = 27;
                var key = e.keyCode || e.which;

                if (key === ESC_KEY) {
                    psClose(slider, param);
                }
            }

            /*
            * Watchers
            * */

            $scope.$watch('psOpen', function(value) {
                if (!!value) {
                    psOpen(slider, param);
                } else {
                    psClose(slider, param);
                }
            });

            $scope.$watch('psSize', function(newValue, oldValue) {
                if (oldValue !== newValue) {
                    param.size = newValue;
                    psOpen(slider, param);
                }
            });

            /*
            * Events
            * */

            $scope.$on('$destroy', function () {
                body.removeChild(slider);
            });

            if ($scope.psAutoClose) {
                $scope.$on('$locationChangeStart', function() {
                    psClose(slider, param);
                });
                $scope.$on('$stateChangeStart', function() {
                    psClose(slider, param);
                });

            }
        }
    };
}]);

/**************************************************************************************************************************************************************/
tmApp.directive('ionGooglePlace', ['$ionicTemplateLoader', '$ionicBackdrop', '$ionicPlatform', '$q', '$timeout', '$rootScope', 'gmService', '$document',
    function($ionicTemplateLoader, $ionicBackdrop, $ionicPlatform, $q, $timeout, $rootScope, gmService, $document) {
        return {
            require: '?ngModel',
            restrict: 'E',
            template: '<input type="text" readonly="readonly" class="dummy" autocomplete="off">',
            replace: true,
            scope: {
                ngModel: '=?',
                geocodeOptions: '='
            },
            link: function(scope, element, attrs, ngModel) {
                var unbindBackButtonAction;

                scope.locations = [];
                scope.courts = [];
                scope.communities = [];
                scope.isLoading = false;

                //var geocoder = new google.maps.Geocoder();
                var searchEventTimeout = undefined;

                var buttonId = attrs.buttonId;

                var POPUP_TPL = [
                    '<div class="ion-google-place-container">',
                        '<div class="bar bar-header item-input-inset">',
                            '<label class="item-input-wrapper">',
                                '<i class="icon ion-ios-search placeholder-icon"></i>',
                                '<input class="google-place-search" autocomplete="off" autocorrect="off" type="search" ng-model="searchQuery" placeholder="' + (attrs.searchPlaceholder || 'Enter an Address, City name or ZIP code') + '">',
                                '<i class="fa fa-circle-o-notch fa-spin" style="font-size: 20px; color:rgba(51, 51, 51, 0.25) !important;" ng-show="isLoading"></i>',
                            '</label>',
                            '<button class="button button-clear fa fa-times-circle" style="font-size: 22px;">',
                                //attrs.labelCancel || 'Cancel',
                            '</button>',
                        '</div>',
                        '<ion-content class="has-header">',
                            '<ion-list>',
                                '<ion-item ng-show="locations.length==0" style="font-size: 14px; border: 0px;">',
                                  'No results',
                                '</ion-item>',
                                '<ion-item ng-repeat="location in locations" type="item-text-wrap" class="item" ng-click="selectLocation(location)">',
                                  '<label class="item header-accordion" style="padding-left:0px;" ng-show="location.name">{{location.name}}</label>',
                                  '<span style="font-size: 14px;">{{location.formatted_address}}</span>',
                                '</ion-item>',
                            '</ion-list>',
                        '</ion-content>',
                    '</div>'
                ].join('');

                var popupPromise = $ionicTemplateLoader.compile({
                    template: POPUP_TPL,
                    scope: scope,
                    appendTo: $document[0].body
                });

                popupPromise.then(function(el){
                    var searchInputElement = angular.element(el.element.find('input'));

                    scope.selectLocation = function(location){
                        ngModel.$setViewValue(location);
                        ngModel.$render();
                        scope.searchQuery = "";
                        if(attrs.searchType==='geocode'){
                          scope.locations = [];
                        }
                        el.element.css('display', 'none');
                        $ionicBackdrop.release();

                        if (unbindBackButtonAction) {
                            unbindBackButtonAction();
                            unbindBackButtonAction = null;
                        }
                    };

                    if(attrs.searchType==='court'){
                      if(!scope.courts || scope.courts.length==0){
                        gmService.tennisClubs()
                        .then(function(results){
                          scope.courts = results;
                          scope.locations = scope.courts;
                        });
                      }
                    }

                    if(attrs.searchType==='community'){
                      if(!scope.communities || scope.communities.length==0){
                        gmService.tennisClubs()
                        .then(function(results){
                          scope.communities = results;
                          scope.locations = scope.communities;
                        });
                      }
                    }

                    scope.$watch('searchQuery', function(query){
                        if (searchEventTimeout){
                          $timeout.cancel(searchEventTimeout);
                        }
                        searchEventTimeout = $timeout(function() {
                          if(attrs.searchType==='geocode'){
                            if(!query || query.length < 3){
                              return;
                            }

                            scope.isLoading = true;
                            
                            var req = scope.geocodeOptions || {};
                            req.address = query;
                            $rootScope.gmGeocoder.geocode(req, function(results, status) {
                              if (status == google.maps.GeocoderStatus.OK) {
                                  scope.$apply(function(){
                                      scope.locations = results;
                                      scope.isLoading = false;
                                  });
                              } else {
                                  // @TODO: Figure out what to do when the geocoding fails
                                scope.$apply(function(){

                                  scope.isLoading = false;
                                });
                              }
                            });
                          }

                          if(attrs.searchType==='court'){
                            if(!query || query.length < 3){
                              scope.locations = scope.courts;
                              return;
                            }

                            scope.isLoading = true;

                            scope.locations = jlinq.from(scope.courts)
                                                    .contains("name", query)
                                                    .or()
                                                    .contains("formatted_address", query)
                                                    .select();

                            scope.isLoading = false;
                          }

                          if(attrs.searchType==='community'){
                            if(!query || query.length < 3){
                              scope.locations = scope.communities;
                              return;
                            }

                            scope.isLoading = true;

                            scope.locations = jlinq.from(scope.communities)
                                                    .contains("name", query)
                                                    .or()
                                                    .contains("formatted_address", query)
                                                    .select();

                            scope.isLoading = false;
                          }

                          $rootScope.scrollTop();

                        }, 350); // we're throttling the input by 350ms to be nice to google's API
                    });

                    var onClick = function(e){
                        e.preventDefault();
                        e.stopPropagation();

                        $ionicBackdrop.retain();
                        unbindBackButtonAction = $ionicPlatform.registerBackButtonAction(closeOnBackButton, 250);

                        el.element.css('display', 'block');
                        searchInputElement[0].focus();
                        setTimeout(function(){
                            searchInputElement[0].focus();
                        },0);
                    };

                    var onCancel = function(e){
                        scope.searchQuery = '';
                        $ionicBackdrop.release();
                        el.element.css('display', 'none');

                        if (unbindBackButtonAction){
                            unbindBackButtonAction();
                            unbindBackButtonAction = null;
                        }
                    };

                    var closeOnBackButton = function(e){
                        e.preventDefault();

                        el.element.css('display', 'none');
                        $ionicBackdrop.release();

                        if (unbindBackButtonAction){
                            unbindBackButtonAction();
                            unbindBackButtonAction = null;
                        }
                    }

                    if(buttonId){
                      $("#"+buttonId).bind('click', onClick);
                      $("#"+buttonId).bind('touchend', onClick);
                    }
                    else{
                      element.bind('click', onClick);
                      element.bind('touchend', onClick);
                    }
                    el.element.find('button').bind('click', onCancel);
                });

                if(attrs.placeholder){
                    element.attr('placeholder', attrs.placeholder);
                }


                ngModel.$formatters.unshift(function (modelValue) {
                    if (!modelValue) return '';
                    return modelValue;
                });

                ngModel.$parsers.unshift(function (viewValue) {
                    return viewValue;
                });

                ngModel.$render = function(){
                    if(!ngModel.$viewValue){
                        element.val('');
                    } else {
                        element.val(ngModel.$viewValue.formatted_address || '');
                    }
                };

                scope.$on("$destroy", function(){
                    if (unbindBackButtonAction){
                        unbindBackButtonAction();
                        unbindBackButtonAction = null;
                    }
                });
            }
        };
    }
]);