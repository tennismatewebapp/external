// Module Code goes here
'use strict';
//************* Main module ***************************
var tmApp = angular.module("tmApp", ['ionic','ngCordova','ui.router','ngSanitize','LocalStorageModule']);

//Define routes
tmApp.config(function ($stateProvider, $urlRouterProvider) {

    $stateProvider

    .state("home", {
        url: "/home",
        controller: "homeController",
        templateUrl: "../app/views/home.html"
    })

    .state("login", {
        url: "/login",
        controller: "loginController",
        templateUrl: "../app/views/login.html"
    })

    .state("signup", {
        url: "/signup",
        controller: "signupController",
        templateUrl: "../app/views/signup.html",
        params: {
            autoActivateChild: "signup.step1"
        }
    })

    .state("signup.step1", {
        url: "/step1",
        templateUrl: "../app/views/templates/signup1.html"
    })

    .state("signup.step2", {
        url: "/step2",
        templateUrl: "../app/views/templates/signup2.html"
    })

    .state("signup.step3", {
        url: "/step3",
        templateUrl: "../app/views/templates/signup3.html"
    })

    .state("resetpassword", {
        url: "/resetpassword",
        controller: "resetPasswordController",
        templateUrl: "../app/views/resetPassword.html"
    })

    .state("forgotpassword", {
        url: "/forgotpassword",
        controller: "resetPasswordController",
        templateUrl: "../app/views/resetPassword.html"
    })

    .state("profile", {
        url: "/profile",
        controller: "profileController",
        templateUrl: "../app/views/profile.html"
    })

    .state("match", {
        url: "/match/:cmdParam",
        controller: "matchController",
        templateUrl: "../app/views/match.html"
    })

    .state("community", {
        url: "/community",
        controller: "communityController",
        templateUrl: "../app/views/community.html"
    })

    .state("mates", {
        url: "/mates",
        controller: "matesController",
        templateUrl: "../app/views/mates.html"
    })

    .state("deviceProps", {
        url: "/deviceprops",
        controller: "deviceController",
        templateUrl: "../app/views/device.html"
    });

    // catch all route
    $urlRouterProvider.otherwise("/profile");
});

//Define interceptor
tmApp.config(function ($httpProvider) {
    $httpProvider.defaults.useXDomain = true;
    delete $httpProvider.defaults.headers.common['X-Requested-With'];
    $httpProvider.interceptors.push('authInterceptorService');
});

//Local storage
tmApp.config(function (localStorageServiceProvider) {
  localStorageServiceProvider
    .setPrefix('tmApp')
    .setStorageType('sessionStorage')
    .setNotify(true, true);
});

tmApp.config(function($ionicConfigProvider) {
  $ionicConfigProvider.views.transition('platform');
});

tmApp.config(function($compileProvider){
  $compileProvider.imgSrcSanitizationWhitelist(/^\s*(https?|ftp|mailto|file|tel):/);
})
/******************************/
//RUN
/******************************/
tmApp.run(['$rootScope', 'authService', 'xhrInterceptorService', '$timeout', '$location', '$state', '$q', 'amsService', 'gmService', 'genericService', 
                        '$ionicScrollDelegate', '$ionicPopup', '$ionicBackdrop', '$ionicPlatform', 'cordova',
    function ($rootScope, authService, xhrInterceptorService, $timeout, $location, $state, $q, amsService, gmService,
                                    genericService, $ionicScrollDelegate, $ionicPopup, $ionicBackdrop, $ionicPlatform, cordova) {


    
    
    //ionic init
    $ionicPlatform.ready(function() {
        // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
        // for form inputs)
        $ionicPlatform.isFullScreen = true;

        if (window.cordova && window.cordova.plugins.Keyboard) {
          cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
          cordova.plugins.Keyboard.disableScroll(true);
        }

        if(window.cordova && window.cordova.plugins && window.cordova.plugins.nativepagetransitions){
            // then override any default you want
            cordova.plugins.nativepagetransitions.globalOptions.duration = 500;
            cordova.plugins.nativepagetransitions.globalOptions.iosdelay = 350;
            cordova.plugins.nativepagetransitions.globalOptions.androiddelay = 350;
            cordova.plugins.nativepagetransitions.globalOptions.winphonedelay = 350;
            cordova.plugins.nativepagetransitions.globalOptions.slowdownfactor = 4;
            // these are used for slide left/right only currently
            cordova.plugins.nativepagetransitions.globalOptions.fixedPixelsTop = 0;
            cordova.plugins.nativepagetransitions.globalOptions.fixedPixelsBottom = 0;
        }

        if (window.StatusBar) {
          // org.apache.cordova.statusbar required
          StatusBar.styleDefault();
        }

        
    });

    //ng-cordova init
/*
    $cordovaLocalNotification.registerPermission().then(function () {
    //alert("registered");
    }, function () {
        //alert("denied registration");
    });

    var iosConfig = {
        "badge": true,
        "sound": true,
        "alert": true
    };

    $cordovaPush.register(iosConfig).then(function (result) {
        //alert("device token: " + result.deviceToken);
    }, function (error) {
        //alert("error " + error);
    });

    $rootScope.$on('$cordovaPush:notificationReceived', function (event, notification) {
        if (notification.alert) {
          navigator.notification.alert(notification.alert);
        }
        if (notification.sound) {
          var snd = new Media(event.sound);
          snd.play();
        }
        if (notification.badge) {
          $cordovaPush.setBadgeNumber(notification.badge).then(function (result) {
            // Success!
          }, function (err) {
            // An error occurred. Show a message to the user
          });
        }
    });


  $rootScope.$on("$cordovaNetwork:offline", function (event, result) {
    alert("Device is now Offline!");
  });


  $rootScope.$on("$cordovaNetwork:online", function (event, result) {
    alert("Device is Online!");
  });

  $rootScope.$on("$cordovaBatteryStatus:status", function (event, status) {
    //alert("status: " + status);
  });*/

/**************************************************************************************/
    //default state
    $rootScope.defaultState = "profile";


    //Automatically load child view in UI-VIEW
    $rootScope.$on('$stateChangeSuccess', function(event, toState){
        if(toState.params && toState.params.autoActivateChild){
            $state.go(toState.params.autoActivateChild);
        }
    });

    // Setup some callbacks for XHR interception
    xhrInterceptorService.addRequestCallback(function(xhr) {
        //console.debug("request",xhr);
        $timeout(function(){ 
            $rootScope.isLoading = true;
        });
    });
    xhrInterceptorService.addResponseCallback(function(xhr) {
        //console.debug("response",xhr);
        $timeout(function(){ 
            $rootScope.isLoading = false;
        });
    });

    // will proxify XHR to fire the above callbacks
    xhrInterceptorService.wire();

/*************************
Default message containers
*************************/
    $rootScope.PopUpConfirm = {
        title:"",
        template:"",
        templateUrl:"",
        show : function() {
            var deferred = $q.defer();
            var confirmPopup = $ionicPopup.confirm({
                title:      this.title,
                template:   this.template,
                tmplateUrl: this.templateUrl
            });
            $ionicBackdrop.retain();
            confirmPopup.then(function(res) {
                if(res) {
                   //console.log('Confirmed');
                } else {
                   //console.log('Declined');
                }
                $ionicBackdrop.release();
                deferred.resolve(res);
            });
            return deferred.promise;
        }
    };

    $rootScope.PopUpAlert = {
        title:"",
        template:"",
        templateUrl:"",
        show : function() {
            var deferred = $q.defer();
            var alertPopup = $ionicPopup.alert({
                title:       this.title,
                template:    this.template,
                templateUrl: this.templateUrl
            });
            $ionicBackdrop.retain();
            alertPopup.then(function(res) {
                //console.log('Alert Closed');
                $ionicBackdrop.release();
                deferred.resolve(res);
            });
            return deferred.promise;
        }
    };

    //Orientation change  
    $rootScope.orientationPortrait = true; 

    window.addEventListener("resize", function() {
        var width = window.innerWidth;
        var height = window.innerHeight;
        if(width > height){
            $rootScope.orientationPortrait = false;
        }
        else{
            $rootScope.orientationPortrait = true;
        }
        $rootScope.$apply();
    }, true); 
    
    //scrolling
    $rootScope.scrollTop = function(){
        $ionicScrollDelegate.scrollTop(true);
    };

    $rootScope.scrollBottom = function(){
        $timeout(function(){
            $ionicScrollDelegate.scrollBottom(true);
        },250);
        
    };

    $rootScope.scrollToAnchorWithinCurrentPage = function(anchor) 
    {
        $location.hash(anchor);
        var handle = $ionicScrollDelegate.$getByHandle('content');
        handle.anchorScroll();
    };

    //ams service
    amsService.init();

    //google maps service
    $rootScope.isLoading = true;
    gmService.init()
    .then(function(){
        $timeout(function(){ 
            $rootScope.isLoading = false;
        });
    }, function(error){
        //TODO: redirect to a error page
        console.log("Error in Google Maps Service!! Error: "+error);
    });

    //Auth
    authService.fillAuthData();

    //Slide Menu
    $rootScope.menuOpen = false; // This will be binded using the ps-open attribute
    $rootScope.toggleMenu = function(){
        $rootScope.menuOpen = !$rootScope.menuOpen;
        $("body").css({
            "overflow" : $rootScope.menuOpen ? "hidden" : "",
            "height"   : $rootScope.menuOpen ? "100%" : ""
        });
    };

}]);

/***************************************************************************************************************************************************************/
//filters & helpers

tmApp.filter( 'titlecase', function() {
    return function( input ) {
        return input.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
    }
});

/***************************************************************************************************************************************************************/
tmApp.filter('propsFilter', function() {
  return function(items, props) {
    var out = [];

    if (angular.isArray(items)) {
      items.forEach(function(item) {
        var itemMatches = false;

        var keys = Object.keys(props);
        for (var i = 0; i < keys.length; i++) {
          var prop = keys[i];
          var text = props[prop].toLowerCase();
          if (item[prop].toString().toLowerCase().indexOf(text) !== -1) {
            itemMatches = true;
            break;
          }
        }

        if (itemMatches) {
          out.push(item);
        }
      });
    } else {
      // Let the output be the input untouched
      out = items;
    }

    return out;
  }
});
/***************************************************************************************************************************************************************/
tmApp.filter('getByProperty', function() {
    return function(propertyName, propertyValue, collection, operator) {
        var i=0, len=collection.length;
        for (; i<len; i++) {
            if(operator){
                if (collection[i][propertyName] == +propertyValue) {
                    return collection[i];
                }
            }
            else{
                if (collection[i][propertyName] != +propertyValue) {
                    return collection[i];
                }
            }
        }
        return null;
    }
});