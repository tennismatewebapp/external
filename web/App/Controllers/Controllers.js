//Controller code goes here...
'use strict';
/*******************************************************************************************************************************************************************/
tmApp.controller('indexController', ['$scope', '$rootScope', '$location', 'authService', '$timeout',
                            function ($scope, $rootScope, $location, authService, $timeout) {


    $rootScope.systemMessage  = {
        error: "",
        info: "",
        warning: "",
        success: "",
        suppressed: false,
        persistent: false    
    };


    $rootScope.logOut = function () {
        authService.logOut();
        $location.path('/login');
    };

    $scope.authentication = authService.authentication;

/*********************************************
//Sys msg watch
/*********************************************/
    $rootScope.$watch('systemMessage.error', function(value) {
        if(value && value.length > 0){
            $rootScope.systemMessage.info = "";
            $rootScope.systemMessage.warning = "";
            $rootScope.systemMessage.success = "";
        }
    });
/*********************************************
//Slide menu watch
/*********************************************/
    $rootScope.$watch('menuOpen', function(value) {
        $scope.menuOpen = value;
    });
    
/*********************************************
//XHR watch
/*********************************************/
    $rootScope.msgFlag = false;
    var hideMessage = function(delay){
        $rootScope.msgFlag=true;
        setTimeout(function(){
            $timeout(function(){
                $rootScope.systemMessage = {};
                $rootScope.msgFlag=false;
            });
        },delay);
    };
    $rootScope.$watch('isLoading', function(loading) {
        $timeout(function(){
            if(loading){
                console.log("loading..");
                if(!$rootScope.systemMessage.suppressed){
                    $rootScope.msgFlag = true;                        
                }
            }
            else if($rootScope.msgFlag && !$rootScope.systemMessage.persistent){
                hideMessage(2500);
                console.log("loading done.");
            }
        });
    });

}]);
/*******************************************************************************************************************************************************************/
tmApp.controller('deviceController', ['$scope', '$rootScope', '$location', '$http', 'amsService', 'stringManipulator', '$timeout', 
    function ($scope, $rootScope, $location, $http, amsService, stringManipulator, $timeout) {

    $scope.data = {
        date: "",
        time: ""
    };

    var options = {
        date: new Date(),
        mode: 'date', // or 'time'
        minDate: new Date() - 10000,
        allowOldDates: true,
        allowFutureDates: false,
        doneButtonLabel: 'DONE',
        doneButtonColor: '#F2F3F4',
        cancelButtonLabel: 'CANCEL',
        cancelButtonColor: '#000000'
  };

  document.addEventListener("deviceready", function () {

    $cordovaDatePicker.show(options).then(function(date){
        alert(date);
    });

  }, false);

}]);

/*******************************************************************************************************************************************************************/
tmApp.controller('profileController', ['$scope', '$rootScope', '$state', '$http', 'authService', 'amsService', 'gmService', 'stringManipulator', '$timeout', 'localStorageService',
                                    function ($scope, $rootScope, $state, $http, authService, amsService, gmService, stringManipulator, $timeout, localStorageService) {

    $rootScope.systemMessage = {};

    $scope.profile = {};
    
    $scope.selectedValues = {
        skill:{},
        skills_expanded: false,
        style:{},
        style_expanded: false,
        cityCountry: "",
        community:{},
        communities_expanded: false,
        court:{},
        courts_expanded: false,
        courtType:{},
        courtTypes_expanded: false,
        match:{},
        matches_expanded: false
    };

    $scope.defaultProfileAvatarUrl = "./img/borat.jpg";
    $scope.defaultCourtAvatarUrl = "./img/avatars/tennis_court_sticker.png";


    $scope.modalSearch = {
        show: false
    };

    $scope.shownGroup = null;

    $scope.editMode = [{
        enabled: false
    }];

/************************************
WATCHERS
*************************************/

    $scope.$watch("editMode", function(value){
        for(var i=0; i<value.length;i++){
            //alert(value[i].enabled);
        }
    },true);

    $scope.$watch("profile", function(value){
        if(!value.avatarUrl){
            value.avatarUrl = $scope.defaultProfileAvatarUrl;
        }


    },true);

    $scope.$watch("selectedValues", function(value){
        if(!value) return;

        if(value.cityCountry && value.cityCountry.address_components){
            var ac = value.cityCountry.address_components;
            var ap = gmService.addressParts(ac);

            $scope.profile.city = ap.city;
            $scope.profile.zip = ap.postal_code;
            $scope.profile.countryState = ap.state; 
            $scope.profile.country = ap.country_long; 
        }

        if(value.courtType){
            $scope.profile.favoriteCourtTypeId = value.courtType.id;
        }

        if(!value.court.avatarUrl){
            value.court.avatarUrl = $scope.defaultCourtAvatarUrl;
        }

        if(!value.community.avatarUrl){
            value.community.avatarUrl = $scope.defaultCourtAvatarUrl;
        }

    },true);

    $scope.$watch("selectedValues.courtTypes_expanded", function(value){
        if(value){
            $scope.selectedValues.courts_expanded = false;
            $scope.selectedValues.communities_expanded = false;
            $scope.selectedValues.matches_expanded = false;
        }
    },true);  

    $scope.$watch("selectedValues.courts_expanded", function(value){
        if(value){
            $scope.selectedValues.courtTypes_expanded = false;
            $scope.selectedValues.communities_expanded = false;
            $scope.selectedValues.matches_expanded = false;
        }
    },true); 

    $scope.$watch("selectedValues.communities_expanded", function(value){
        if(value){
            $scope.selectedValues.courts_expanded = false;
            $scope.selectedValues.courtTypes_expanded = false;
            $scope.selectedValues.matches_expanded = false;
        }
    },true); 

    $scope.$watch("selectedValues.matches_expanded", function(value){
        if(value){
            $scope.selectedValues.courts_expanded = false;
            $scope.selectedValues.communities_expanded = false;
            $scope.selectedValues.courtTypes_expanded = false;
        }
    },true); 

    $scope.$watch("selectedValues.court", function(value){
        value.courtTypeId = 6; //NA
        value.verified = 1;
        value.createdBy = authService.userId();
        if(value.formatted_address && value.geometry){
            value.addressLine1 = value.formatted_address.split(',')[0];
            value.lat = value.geometry.location.lat();
            value.lon = value.geometry.location.lng();
        }
        if(value.photos){

        }
        if(value.place_id){
            gmService.placeDetails(value.place_id)
            .then(function(results){ 
                value.zip = results.postal_code;           
                value.city = results.city;
                value.country = results.country_short;
                $scope.selectedValues.court.club = $scope.selectedValues.court.club || {};
                $scope.selectedValues.court.club.name = value.name;
                $scope.selectedValues.court.club.avatarUrl = value.avatarUrl;
                $scope.selectedValues.court.club.addressLine1 = value.addressLine1;
                $scope.selectedValues.court.club.zip = value.zip;
                $scope.selectedValues.court.club.city = value.city;
                $scope.selectedValues.court.club.country = value.country;
                $scope.selectedValues.court.club.lat = value.lat;
                $scope.selectedValues.court.club.lon = value.lon;
                $scope.selectedValues.court.club.url = !results.website ? results.url : results.website;
                if(results.phone){
                    $scope.selectedValues.court.club.phone = results.phone;
                }
            });
        }
    });

    $scope.$watch("selectedValues.community", function(value){
        value.createdBy = authService.userId();
        if(value.formatted_address && value.geometry){
            value.addressLine1 = value.formatted_address.split(',')[0];
            value.lat = value.geometry.location.lat();
            value.lon = value.geometry.location.lng();
        }
        if(value.place_id){
            gmService.placeDetails(value.place_id)
            .then(function(results){ 
                value.zip = results.postal_code;           
                value.city = results.city;
                value.country = results.country_short;
                value.url = !results.website ? results.url : results.website;
                if(results.phone){
                    value.phone = results.phone;                    
                }
            });
        }
    });


    $scope.initProfile = function(){
        //profile
        authService.userDetails()
        .then(function(response){
            $scope.profile = $rootScope.user_details;
        });
        
        //skill level
        var query = amsService.table("Skill")
                    .select("id", "skillLevel", "name", "description", "avatarUrl")
                    .orderBy("skillLevel")
                    .read().then(function(data){
                        $scope.skills=data;
                    },function(error){
                        $rootScope.systemMessage.error = error;
                    });
        console.clear();

        //year of birth
        $scope.years = [];
        var minAge = 10;
        var curYear = new Date().getFullYear()-minAge;
        for(var x = curYear; x > curYear-90; x--){
            var tmp = {
                id:x,
                text:x
            };
            $scope.years.push(tmp);
        }

        //playing style
        var query = amsService.table("PlayingStyle")
                    .select("id", "name", "description", "avatarUrl")
                    .orderBy("id")
                    .read().then(function(data){
                        $scope.styles=data;
                    },function(error){
                        $rootScope.systemMessage.error = error;
                    });

        console.clear();

        //court types
        var query = amsService.table("CourtType")
                    .select("id", "name", "avatarUrl")
                    .orderBy("id")
                    .read().then(function(data){
                        $scope.courtTypes=data;
                    },function(error){
                        $rootScope.systemMessage.error = error;
                    });

        console.clear();

        //clubs
        var query = amsService.table("Clubs")
                    .read().then(function(data){
                        $scope.clubs=data;
                    },function(error){
                        $rootScope.systemMessage.error = error;
                    });

        console.clear();

        
    };

    $scope.groups = ["personalInfo","skills","favorites"];

    $scope.toggleGroup = function(group) {
        if ($scope.isGroupShown(group)) {
          $scope.shownGroup = null;
        } else {
          $scope.shownGroup = group;
        }
        $rootScope.scrollTop();
    };
    $scope.isGroupShown = function(group) {
        return $scope.shownGroup === group;
    };

}]);
/*******************************************************************************************************************************************************************/

tmApp.controller('loginController', ['$rootScope','$scope', '$location', 'authService', '$timeout', 'genericService',
                    function ($rootScope, $scope, $location, authService, $timeout, genericService) {

    $scope.loginData = {
        userName: "",
        pwd: "",
        rememberMe: false
    };

    $rootScope.systemMessage = {};
    

    $scope.login = function () {
        $rootScope.systemMessage.persistent = true;
        $rootScope.systemMessage.info = "Verifying user...";
        authService.login($scope.loginData)
        .then(function (response) {
            $rootScope.systemMessage.info = "Verified.";
            $rootScope.systemMessage.persistent = false;
            genericService.Redirect($rootScope.defaultState, 1500);
        }, function (err) {
             $timeout(function(){
                $rootScope.systemMessage.error = "Login failed...please try again or sign up.";
             });
            setTimeout(function(){
                $rootScope.systemMessage.persistent = false;
                $rootScope.msgFlag = false;
                $rootScope.$apply();
            },5500);
                
             $scope.clearLoginData();
         });
    };

    $scope.clearLoginData = function(){
        //$scope.loginform.$setPristine();
        $scope.loginData = {
            userName: "",
            pwd: "",
            rememberMe: false
        };
    };

}]);
/*******************************************************************************************************************************************************************/

tmApp.controller('homeController', ['$scope', '$location', 'authService', function ($scope, $location, authService) {


}]);
/*******************************************************************************************************************************************************************/

tmApp.controller('communityController', ['$scope', '$location', 'authService', function ($scope, $location, authService) {


}]);
/*******************************************************************************************************************************************************************/

tmApp.controller('signupController', ['$scope', '$rootScope', '$location','$state', '$timeout', 'authService', '$http', 'stringManipulator', 'amsService', 
                                        'genericService','$ionicScrollDelegate', '$anchorScroll',
                                    function ($scope, $rootScope, $location, $state, $timeout, authService, $http, stringManipulator, 
                                                                        amsService, genericService, $ionicScrollDelegate, $anchorScroll) {

    $rootScope.systemMessage = {};
    
    var self = this;
    $scope.$state = $state;

    //validation flags
    $scope.step1Valid = false;
    $scope.step2Valid = false;
    $scope.step3Valid = false;

    $scope.userExists = false;

    $scope.registration = {
        email: "",
        pwd: "",
        confirmPwd: "",
        firstName: "",
        lastName: "",
        nick: "",
        skillId: "",
        playStyleId: "",
        yearOfBirth: "",
        gender: "0",
        city: "",
        countryState:"",
        country:"",
        countryId: "",
        eulaAgree: false
    };

    $scope.groups = ["skills", "playing-style", "city-country"];

    $scope.eula = {
        id: 0,
        content:""
    };

    $scope.addressFound = false;

    $scope.selectedValues = {
        skill:"",
        style:"",
        cityCountry: ""
    };

    $scope.$watch('selectedValues.skill', function(value) {
        if(value){
            $scope.registration.skillId = value.id; 
            $("input[name=skillId]+.header-accordion>span").text('Tennis Level: '+value.skillLevel); 
        }     
    });

    $scope.$watch('selectedValues.style', function(value) {
        if(value){
            $scope.registration.playStyleId = value.id; 
            $("input[name=playStyleId]+.header-accordion>span").text('Playing Style: '+value.name); 
        }     
    });

    $scope.$watch('selectedValues.cityCountry', function(value) {
        if(!value) return;

        if(value.address_components){
            var ac = value.address_components;
            var countryCode;

            for(var i=0;i<ac.length;i++){
                if(ac[i].types[0]==='locality'){
                    $scope.registration.city = ac[i].long_name;
                }

                if(ac[i].types[0]==='country'){
                    $scope.registration.country = ac[i].long_name;
                    countryCode = ac[i].short_name; 
                }

                if(ac[i].types[0]==='administrative_area_level_1'){
                    $scope.registration.countryState = ac[i].long_name; 
                }
            }

            $rootScope.systemMessage.suppressed = true;
            var query = amsService.table("Countries")
                    .where({code: countryCode})
                    .select("id")
                    .read()
                    .then(function(data){
                        if(data.length>0){
                            $scope.registration.countryId = data[0].id;
                        }
                        $rootScope.systemMessage.suppressed = true;
                    });
            console.clear();
        }
        
                 
    }, true);

    $scope.initSignup = function(){
        
        //skill level
        var query = amsService.table("Skill")
                    .where({deletedDate: null})
                    .select("id", "skillLevel", "name", "description", "avatarUrl")
                    .orderBy("skillLevel")
                    .read().then(function(data){
                        $scope.skills=data;
                    },function(error){
                        $rootScope.systemMessage.error = error;
                    });
        console.clear();

        //year of birth
        $scope.years = [];
        var curYear = new Date().getFullYear()-10;
        for(var x = curYear; x > curYear-90; x--){
            $scope.years.push(x);
        }

        //playing style
        var query = amsService.table("PlayingStyle")
                    .where({deletedDate: null})
                    .select("id", "name", "description", "avatarUrl")
                    .orderBy("id")
                    .read().then(function(data){
                        $scope.styles=data;
                    },function(error){
                        $rootScope.systemMessage.error = error;
                    });

        console.clear();

        //eula
        var query = amsService.table("EULA");
        query.read().then(function(data){
            var temp = $.map(data, function(item) {
                $scope.eula.id = item.id;
                $scope.eula.content = item.content
            });
        }, function(error){
            $rootScope.systemMessage.error = error;
        });
        console.clear();
    };

    //format
    $scope.parseInputs = function(){
        $scope.registration.firstName = stringManipulator.toTitleCase($scope.registration.firstName);
        $scope.registration.lastName = stringManipulator.toTitleCase($scope.registration.lastName);
    };

    $scope.goPrevious = function(){
        if($state.current.name==="signup.step2"){
            $state.go("signup.step1");
        }
        else if($state.current.name==="signup.step3"){
             $state.go("signup.step2");
        }
    };

    $scope.goNext = function(){
        if($state.current.name==="signup.step1"){
            validateStep1($scope.registration.pwd, $scope.registration.confirmPwd);
        }
        else if($state.current.name==="signup.step2"){
            validateStep2();
        }
        else if($state.current.name==="signup.step3"){
            validateStep3();
            if($scope.step3Valid){
                $scope.signUp();
            }
        }
    };

    var validateStep1 = function(pwd1, pwd2){

        $scope.step1Valid = false;
        var elmEmail = $scope.form.signUpForm1.email;

        if(!validatePwd()){
            var pwdConfirm = $scope.form.signUpForm1.pwd2;
            pwdConfirm.$setValidity('required', false);
            $('input[name='+pwdConfirm.$name+']').attr("placeholder", "Passwords don't match.");
            $scope.registration.confirmPwd = "";

            return;
        }
        else if(!$scope.registration.email || $scope.registration.email.length==0){
            elmEmail.$setValidity('required', false);
            $('input[name='+elmEmail.$name+']').attr("placeholder", "Enter E-mail here.");

            return;
        }

        var promiseExists = authService.userExists($scope.registration.email);
        promiseExists.then(function(response){
            $scope.userExists = response;
            if($scope.userExists){
                $timeout(function(){
                    elmEmail.$setValidity('email', false);
                    $('input[name='+elmEmail.$name+']').attr("placeholder", "User allready exists.");
                    $scope.registration.email = "";
                });

            }
            else{
                $scope.step1Valid = true;
                $state.go("signup.step2");
            }
        });       
    };

    var validateStep2 = function(){
        if($scope.registration.skillId.length>0 &&
            $scope.registration.playStyleId.length>0 && 
            $scope.registration.yearOfBirth.toString().length > 0 && 
            $scope.registration.city.length > 0 && 
            $scope.registration.countryId.length>0){

            if($scope.registration.nick.length==0){
                $scope.registration.nick = $scope.registration.firstName;
            }

            $scope.step2Valid = true;
            $state.go("signup.step3");
        }
        else{
            $scope.step2Valid = false;
        }
    };

    var validateStep3 = function(){
        $scope.step3Valid = $scope.registration.eulaAgree && 
                            $scope.registration.pwd.length>0;
    };

    $scope.signUp = function () {
        authService.saveRegistration($scope.registration).then(function (response) {
            console.log("User registered. ID: "+response.data);
            $rootScope.systemMessage.success = "User has been registered successfully, you will be redicted to login page in 2 seconds.";
            genericService.Redirect("login", 2000)

        },function (error) {
            var tmp = {};
            tmp = JSON.parse(error);
            $rootScope.systemMessage.error = "Error: " + tmp.message;
         });
    };


    $scope.setFormScope = function(obj){
        $scope.form = obj;
    };


    $scope.toggleGroup = function(group) {
        if ($scope.isGroupShown(group)) {
          $scope.shownGroup = null;
        } else {
          $scope.shownGroup = group;
        }
    };

    $scope.isGroupShown = function(group) {
        return $scope.shownGroup === group;
    };

    function validatePwd (){
        if($scope.registration.pwd != $scope.registration.confirmPwd){
            var msg;
            switch(window.navigator.language){
                case "da":
                    msg = "Kodeordene stemmer ikke overens!"
                break;
                case "en":
                    msg = "Passwords don't match!";
                break;
                case "de":
                    msg = "Passwörter stimmen nicht überein!"
                break;
                case "fr":
                    msg = "Les mots de passe ne correspondent pas"
                break;

                case "sl":
                    msg = "Gesla se ne ujemata";
                break;
                case "zu":
                    msg = "Amaphasiwedi awafani";
                break;
                default:
                    msg = "Passwords don't match!";
                break;
          }
            return false
        }
        else{
            return true
        }
    }

    $scope.showEula = function(){
        var popup = $rootScope.PopUpAlert;
        popup.title = "EULA";
        popup.template = $scope.eula.content;
        popup.show();        
    };


}]);
//*******************************************************************************************************************************************************************/

tmApp.controller('resetPasswordController', ['$scope', '$rootScope','$timeout','$location', 'authService', 'genericService', 
                                            function ($scope, $rootScope, $timeout, $location, authService, genericService) {

    $scope.resetPasswordErrMessage = "";
    $scope.resetPasswordMessage = "";

    $scope.resetPasswordData = {
        email: ""
    }

    
    $scope.resetPwd = function(){
        authService.resetPassword($scope.resetPasswordData.email).then(function(response){
            $rootScope.isLoading = false;
            $scope.resetPasswordMessage = "Your password is reset now.";
            genericService.Redirect("login", 5000);
        },function(error){
            $scope.resetPasswordMessage = "";
            $scope.resetPasswordErrMessage = "Error: " + error.message;
            hideMessage(5000);
        });
    };
    
}]);
//*******************************************************************************************************************************************************************/

tmApp.controller('matchController', ['$rootScope', '$location', '$scope', '$timeout', '$stateParams', 'genericService', 'amsService',
                                    'authService', 'gmService', 'stringManipulator',
                                    function ($rootScope, $location, $scope, $timeout, $stateParams, genericService, amsService, 
                                                authService, gmService, stringManipulator) {

    $scope.cmdParam = $stateParams.cmdParam;
    $scope.tab = 1;

    $scope.matchData = {
        courtId:"",
        matchTime: dateNow(),
        responseTime: DateAdd(dateNow(this.matchTime), "d", -1),
        userRequestId: authService.userId(),
        userResponseId:"",
        matchStatusId:""
    };

    $scope.defaultOpponentAvatarUrl = "./img/borat.jpg";
    $scope.defaultCourtAvatarUrl = "./img/avatars/tennis_court_sticker.png";

    $scope.selectedValues = {
        player: {
            id:authService.userId(),
            nickName: "",
            name:""
        },
        invitation:{
            enabled:false,
            id:"",
            invitationTypeId:"",
            invitationType:"",
            createdBy:authService.userId(),
            invitedUserId:"",
            statusId:"",
            status:"",
            private: 1,
            expirationDate: DateAdd(dateNow(), "d", 1),
            responseDueDate: "",
            responseHours:"",
            discussion:{
                invitationId:"",
                createdBy: authService.userId(),
                message: ""
            }
        },
        opponent: {
            id:"",
            nickName: "",
            name:"",
            age:"",
            skillLevel:"",
            playingStyle:"",
            avatarUrl: ""
        },
        court: {
            id: "",
            courtTypeId: "",
            name:"",
            verified: "",
            addressLine1:"",
            zip:"",
            city:"",
            country: "",
            createdBy: authService.userId(),
            avatarUrl:"",
            lat: "",
            lon: "",
            club: 
            {
                name:"",
                avatarUrl:"",
                addressLine1:"",
                zip:"",
                city:"",
                country:"",
                lat:"",
                lon:"",
                url:"",
                phone:"",
                createdBy: authService.userId()
            }
        }
    };

    $scope.responseHours = [
        {value: 3, text: "3 Hours"},
        {value: 6, text: "6 Hours"},
        {value: 12, text: "12 Hours"},
        {value: 16,  text: "16 Hours"}
    ];

    $scope.opponentCandidates = {};
    $scope.filteredOpponentCandidates = {};

    $scope.courts = {};
    $scope.filteredCourts = {};

    $scope.search = {
        query: "",
        enabled: false,
        type: "",
        show: function(argType){
            if(!argType){
                this.enabled = false;
            }
            else{
                this.enabled = true;
            }
            
            this.type = argType;
            this.query="";
        }
    };

    $scope.details =  {
        enabled: false,
        type: "",
        openForm: false,
        show: function(argType){
            if(!argType){
                this.enabled = false;
                this.openForm = false;
            }
            else{
                this.type = argType;
                this.enabled = true;
                this.openForm = true;
            }
        }
    };

/*****************************
    Watchers
*****************************/
    $scope.$watch("search.enabled", function(value){
        if(value){
            if($scope.search.type==="court"){
                if(angular.equals({}, $scope.courts)){
                    $rootScope.systemMessage.info = "Searching tennis courts nearby...";
                    gmService.tennisClubs()
                    .then(function(results){
                        $scope.courts = results;
                        $scope.filteredCourts = $scope.courts;
                        $rootScope.systemMessage.info = "Done.";
                        $rootScope.systemMessage.suppressed = true;
                        $rootScope.systemMessage.suppressed = false;
                    },function(error){
                        $rootScope.systemMessage.error = error;
                    });
                }
                if(angular.equals({}, $scope.filteredCourts)){
                    $scope.filteredCourts = $scope.courts;
                }
                else{
                    if($scope.filteredCourts.length != $scope.courts.length){
                        $scope.filteredCourts = $scope.courts;
                    }
                }
                
            }

            if($scope.search.type==="opponent"){
                if(angular.equals({}, $scope.opponentCandidates)){
                    $rootScope.systemMessage.info = "Fetching data...";
                    var tmp = {};
                    var res = amsService.invokeAPI("friends/"+authService.userId(), "get", null, tmp)
                        .then(function(data){
                            $rootScope.systemMessage.info = "Done.";
                            $scope.opponentCandidates = data;
                            $scope.filteredOpponentCandidates = $scope.opponentCandidates;

                        },function(error){
                            $rootScope.systemMessage.error = error;
                        });
                }
                if(angular.equals({}, $scope.filteredOpponentCandidates)){
                    $scope.filteredOpponentCandidates = $scope.opponentCandidates;
                }
                else{
                    if($scope.filteredOpponentCandidates.length != $scope.opponentCandidates.length){
                        $scope.filteredOpponentCandidates = $scope.opponentCandidates;
                    }
                }

            }
        }
        $rootScope.scrollTop();
    }, true);
    
    $scope.$watch("details.enabled", function(value){
        if(!value){
            $scope.details.type="";
            return;
        }
    }, true);

    $scope.$watch("selectedValues.invitation.enabled", function(value){
        if(value){
            authService.userDetails()
            .then(function(response){
                $scope.selectedValues.player.nickName = response.nick;
                $scope.selectedValues.player.name = response.firstName+" "+response.lastName;
                $scope.selectedValues.invitation.discussion.message = "Hello "+ $scope.selectedValues.opponent.nickName +",\n\n"+
                                                                        "I'd like to invite you to a match on "+moment($scope.selectedValues.invitation.expirationDate).format("llll")+".\n"+
                                                                        "Court: "+$scope.selectedValues.court.name+",\n"+
                                                                        "Address: "+$scope.selectedValues.court.addressLine1+",\n"+
                                                                        "Note: Please respond at latest "+moment($scope.selectedValues.invitation.responseDueDate).format("llll")+"\n\n"+
                                                                        "Best regards,\n "+$scope.selectedValues.player.name;

            });
        }
    }, true);

    $scope.$watch("selectedValues.invitation.expirationDate", function(value){
        if(!value || angular.equals({}, value)) return;
        $scope.selectedValues.invitation.responseDueDate = DateAdd(dateNow(value), "d", -1);
    }, true);

    $scope.$watch("selectedValues.invitation.responseHours", function(value){
        if(!value || angular.equals({}, value)) return;
        var duration = moment.duration({'hours': $scope.selectedValues.invitation.responseHours});
        $scope.selectedValues.invitation.responseDueDate = DateAdd(dateNow(value), "d", -1);
    }, true);

    $scope.$watch("selectedValues.opponent.id", function(value){
        $scope.selectedValues.invitation.invitedUserId = value;
    }, true);

    $scope.$watch("selectedValues.opponent", function(value){
        if(angular.equals({}, value)) return;
        
        if(!value.avatarUrl || value.avatarUrl.length==0){
            value.avatarUrl = $scope.defaultOpponentAvatarUrl;
        }

    },true);

    $scope.$watch("selectedValues.court", function(value){
        if(angular.equals({}, value)) return;
        
        if(!value.avatarUrl || value.avatarUrl.length==0){
            value.avatarUrl = $scope.defaultCourtAvatarUrl;
        }
    },true);

    $scope.$watch("opponentCandidates", function(value){
        if(!value || !value[0]) return;
        for(var i=0; i<value.length;i++){
            if(!value[i].avatarUrl){
                value[i].avatarUrl = $scope.defaultOpponentAvatarUrl;
            }
        }
    },true);

    $scope.$watch("filteredCourts", function(value){
        if(!value || !value[0]) return;
        for(var i=0; i<value.length;i++){
            if(!value[i].avatarUrl){
                value[i].avatarUrl = $scope.defaultCourtAvatarUrl;
            }
        }
    },true);

    function dateNow(value){
        if(!value || value.length == 0){
            return new Date(moment().local().format("YYYY MM D HH:mm"));
        }
        return new Date(moment(value).local().format("YYYY MM D HH:mm"));
    };

    function DateAdd(date, type, amount){
        var y = date.getFullYear(),
            m = date.getMonth(),
            d = date.getDate();
        if(type === 'y'){
            y += amount;
        };
        if(type === 'm'){
            m += amount;
        };
        if(type === 'd'){
            d += amount;
        };
        return new Date(y, m, d);
    };


    $scope.Register = function(){
        var tmp = {};
        var pack = stringManipulator.pack($scope.selectedValues);

        amsService.invokeAPI("invitation?type=match","post", pack, tmp)
        .then(function(response){
            $rootScope.systemMessage.info = "Invitation sent.";
        }, function(error){
            $rootScope.systemMessage.error = "Invitation could not be sent.";
        });

        
    };

    $scope.searchOpponents = function(query){
        if(!query || query.length < 2){
            $scope.filteredOpponentCandidates = $scope.opponentCandidates;
            return;
        }

        $scope.filteredOpponentCandidates = jlinq.from($scope.opponentCandidates)
                                .contains("email", query)
                                .or()
                                .contains("firstName", query)
                                .or()
                                .contains("lastName", query)
                                .or()
                                .contains("nickName", query)
                                .select();
        if(angular.equals({}, $scope.filteredOpponentCandidates)){
            $scope.filteredOpponentCandidates = $scope.opponentCandidates;
        }

        $rootScope.scrollTop();
    };

    $scope.addOpponent = function(opponent){
        $scope.selectedValues.opponent.id = opponent.friendId;
        $scope.selectedValues.opponent.nickName = opponent.nickName;
        $scope.selectedValues.opponent.name = opponent.firstName+" "+opponent.lastName;
        $scope.selectedValues.opponent.age = opponent.age;
        $scope.selectedValues.opponent.avatarUrl = opponent.avatarUrl;
        $scope.selectedValues.opponent.skillLevel = opponent.skillLevel;
        $scope.selectedValues.opponent.playingStyle = opponent.playingStyle;

        $scope.search.enabled = false;
    };


    $scope.searchCourts = function(query){
        if(!query){
            $scope.filteredCourts = $scope.courts;
            return;
        }
        if(query.length < 3){
            $scope.filteredCourts = $scope.courts;
            return;
        }
        
        $scope.filteredCourts = jlinq.from($scope.courts)
                                .contains("name", query)
                                .or()
                                .contains("formatted_address", query)
                                .select();
        if(angular.equals({}, $scope.filteredCourts)){
            $scope.filteredCourts = $scope.courts;
        }

        $rootScope.scrollTop();
    };

    $scope.addCourt = function(court){
        $scope.selectedValues.court.courtTypeId = 6; //NA
        $scope.selectedValues.court.name = court.name;
        $scope.selectedValues.court.verified = 1;
        $scope.selectedValues.court.addressLine1 = court.formatted_address;
        $scope.selectedValues.court.lat = court.geometry.location.lat();
        $scope.selectedValues.court.lon = court.geometry.location.lng();

        gmService.placeDetails(court.place_id)
        .then(function(results){ 
            $scope.selectedValues.court.zip = results.postal_code;           
            $scope.selectedValues.court.city = results.city;
            $scope.selectedValues.court.country = results.country_short;

            if(results.phone){
                $scope.selectedValues.court.club.name = court.name;
                $scope.selectedValues.court.club.avatarUrl = $scope.selectedValues.court.avatarUrl;
                $scope.selectedValues.court.club.addressLine1 = $scope.selectedValues.court.addressLine1;
                $scope.selectedValues.court.club.zip = $scope.selectedValues.court.zip;
                $scope.selectedValues.court.club.city = $scope.selectedValues.court.city;
                $scope.selectedValues.court.club.country = $scope.selectedValues.court.country;
                $scope.selectedValues.court.club.lat = $scope.selectedValues.court.lat;
                $scope.selectedValues.court.club.lon = $scope.selectedValues.court.lon;
                $scope.selectedValues.court.club.url = !results.website ? results.url : results.website;
                $scope.selectedValues.court.club.phone = results.phone;
            }

            $scope.search.enabled = false;
        });
    };

}]);

//*******************************************************************************************************************************************************************/
tmApp.controller('matesController', ['$rootScope', '$location', '$scope', '$timeout', '$q', '$filter', 'amsService', 'authService', 'stringManipulator',
                '$ionicPopup', '$ionicBackdrop',
                function ($rootScope, $location, $scope, $timeout, $q, $filter, amsService, authService, stringManipulator, $ionicPopup, $ionicBackdrop) {

    $scope.tabs = {
        currentTab:1
    };

    $scope.data = {
        email:"",
        nickname:"",
        firstName:"",
        lastName:"",
        avatarUrl:"",
        friendId:"",
        userId:"",
        city:"",
        country:"",
        friendTypeId:1
    };


    $scope.editMode = [{
        enabled: false
    }];

    $scope.defaultAvatarUrl = "../../img/borat.jpg";

    $scope.search = {
         query: "",
         enabled: false
     };
    $scope.myMates = [{}];
    $scope.friendTypes = {};
    $scope.mateCandidates = {};
    $scope.selectedMates = [];

    $scope.$watch("tabs.currentTab", function(value){
        if(value===1){
            $scope.search.enabled = (myMates.length > 5);
        }
        if(value===2){
            $scope.mateCandidates = {};
            $scope.search.enabled = true;
        }
        $timeout(function(){
            $scope.search.query = "";    
        });
        
    },true);


    $scope.$watch("myMates", function(value){
        if(!value || !value[0]) return;
        for(var i=0; i<value.length;i++){
            if(!value[i].avatarUrl){
                value[i].avatarUrl = $scope.defaultAvatarUrl;
            }
        }
        $scope.editMode = [{
            enabled: false
        }];
    },true);

    $scope.$watch("mateCandidates", function(value){
        if(!value || !value[0]) return;
        for(var i=0; i<value.length;i++){
            if(!value[i].avatarUrl){
                value[i].avatarUrl = $scope.defaultAvatarUrl;
            }
        }
        

    },true);

    $scope.$watch("editMode", function(value){
        for(var i=0; i<value.length;i++){
            //value[i].enabled=false;
        }
    },true);

    $scope.initMates = function(){
        var query = amsService.table("friendtype")
                    .select("id", "name", "description", "avatarUrl")
                    .orderBy("id")
                    .read().then(function(data){
                        $scope.friendTypes=data;
                    },function(error){
                        $rootScope.systemMessage.error = error;
                    });
        var tmp = {};
        var query = amsService.invokeAPI("friends/"+authService.userId(), "get", null, tmp)
                    .then(function(data){
                        $scope.myMates = data;
                    },function(error){
                        $rootScope.systemMessage.error = error;
                    });
    };

    $scope.searchMates = function(query){
        if(!query || query.length < 2){
            $scope.mateCandidates = {};
            return;
        }
        var tmp = {};
        $rootScope.systemMessage.suppressed = true;
        var res = amsService.invokeAPI("friends/candidates?id="+authService.userId()+"&candidatequery="+query, "get", null, tmp)
                    .then(function(data){
                        $scope.mateCandidates = data;
                        $rootScope.systemMessage.suppressed = false;
                    },function(error){
                        $rootScope.systemMessage.suppressed = false;
                        $rootScope.systemMessage.error = error;
                    });
    };

    $scope.selectMate = function(){

    };

    $scope.addMate = function(friend){
        var res = $scope.showPopup('Add Player to Mates')
                .then(function(result){
                    if(result){
                        //console.log("Adding mate...");
                        friend.friendId=friend.userId;
                        friend.userId=authService.userId();
                        friend.friendTypeId = $scope.data.friendTypeId;

                        var pack = stringManipulator.pack(friend);
                        var tmp = {};
                        var query = amsService.invokeAPI("friends/"+authService.userId(), "post", pack, tmp)
                                    .then(function(data){
                                        $scope.myMates.push(friend);
                                        var criteria = friend.friendId.toString();
                                        $scope.mateCandidates = $filter("filter")($scope.mateCandidates, {friendId:"!"+criteria});

                                    },function(error){
                                        $rootScope.systemMessage.error = error;
                                    });
                    }
                    else{
                        return;
                    }
                });

    };

    $scope.removeMate = function(friend){
        var popup = $rootScope.PopUpConfirm;
        popup.title = 'Remove Player Confirmation';
        popup.template = 'Are you sure you want to remove this player from Mates?';
        var res = popup.show()
                .then(function(result){
                    if(result){
                        //console.log("Removing mate...");
                        var pack = stringManipulator.pack(friend.friendId);
                        var tmp = {};
                        var query = amsService.invokeAPI("friends/remove/"+authService.userId(), "post", pack, tmp)
                            .then(function(data){
                                var criteria = friend.friendId.toString();
                                $scope.myMates = $filter("filter")($scope.myMates, {friendId:"!"+criteria});
                            },function(error){
                                $rootScope.systemMessage.error = error;
                            });
                    }
                    else{
                        return;
                    }
                });
    };

    $scope.editMate = function(friend){
        if(!$scope.editMateMode){
            $scope.editMateMode = true;
            return;
        }

        $scope.removeMate(friend);
    };

    $scope.isFriend = function(friend){
        var tmp = {};
        var query = amsService.invokeAPI("friends/isfriend?id="+authService.userId()+"&friendid="+friend.userId, "get", null, tmp)
                    .then(function(data){
                        return data;
                    },function(error){
                        $rootScope.systemMessage.error = error;
                    });

        return false;
    };


    $scope.showPopup = function(argtitle, argtemplate){
        var deferred = $q.defer();
        var myPopup = $ionicPopup.show({
            template:   '<label class="item">'+
                        '<h3>How do you know this player?</h3>'+
                        '</label>'+
                        '<label class="item item-input item-select">'+
                            '<div class="input-label">'+
                                '&nbsp;'+
                            '</div>'+
                            '<select ng-model="data.friendTypeId" ng-options="friendType.id as friendType.name for friendType in friendTypes" style="width: 100%; max-width: 100%; background: #f8f8f8;color: #555;">'+
                            '</select>'+
                        '</label>',
            title: argtitle,
            scope: $scope,
            buttons: [
              { text: 'Cancel' },
              {
                text: 'Add player',
                type: 'button-positive',
                onTap: function(e) {
                  if (!$scope.data.friendTypeId) {
                    //don't allow the user to close unless there is some value
                    e.preventDefault();
                  } else {
                    return true;
                  }
                }
              }
            ]
        });
        myPopup.then(function(res) {
            console.log('Tapped!', res);
            deferred.resolve(res);
        });
        /*$timeout(function() {
            myPopup.close(); //close the popup after 3 seconds for some reason
        }, 3000);*/

        return deferred.promise;
    };

}]);